/*
    Virtual VFAT image support (shadows a local directory)
    Ported from Bochs Project

    Copyright (C) 2004,2005 by Johannes E. Schindelin (QEmu)
    Copyright (C) 2010-2012  The Bochs Project
    Copyright (C) 2022 by Philippe Rimauro
*/

/* /// "Includes" */

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/hashtable.h>

#include <hardware/byteswap.h>
#include <libraries/hashtable.h>

#include <string.h>

#include "vvfat.h"

#ifdef __HAIKU__
#include <assert.h>
#endif

extern struct Library *SysBase;
extern struct Library *DOSBase;
extern struct Library *UtilityBase;
extern struct Library *HashTableBase;

/* /// */

/* /// "Defines" */

// Shall be 11 characters long, uppercase, etc.
#define FAT_VOLUME_LABEL "AMIGA VVFAT"

/* Boot sector OEM name. see related compatibility problems at:
 * https://jdebp.eu/FGA/volume-boot-block-oem-name-field.html
 * http://seasip.info/Misc/oemid.html
 */
#define BOOTSECTOR_OEM_NAME "MSWIN4.1"
//#define BOOTSECTOR_OEM_NAME "MSDOS5.0"

// Special sectors numbers
#define MBR_SECTOR(v)         (0)
#define BOOT_SECTOR(v)        ((v)->offset_to_bootsector + 0)
#define BOOT_SECTOR_BACKUP(v) ((v)->offset_to_bootsector + 6)
#define INFO_SECTOR(v)        ((v)->offset_to_bootsector + 1)

// Number of FAT to create
#define NUMBER_OF_FAT 2

// MBR partitions types
#define PARTITION_TYPE_FAT12  0x1
#define PARTITION_TYPE_FAT16  0x6
#define PARTITION_TYPE_FAT16L 0xe
#define PARTITION_TYPE_FAT32  0xb
#define PARTITION_TYPE_FAT32L 0xc

// MBR partition attributes
#define PARTITION_ATTRIBUTE_BOOTABLE 0x80

// FAT characters to exclude from file short name
#define FAT_EXCLUDE_CHAR "+.,;=[]/*:<>|\\\"\?"
// Character to use to replace excluded ones when encountered
// (we do not use '_' because it is not allowed by AMSDOS)
#define FAT_REPLACE_CHAR '^'

// FAT entries attributes
#define FAT_ATTR_READ_ONLY 0x01
#define FAT_ATTR_HIDDEN    0x02
#define FAT_ATTR_SYSTEM    0x04
#define FAT_ATTR_VOLUME_ID 0x08
#define FAT_ATTR_DIRECTORY 0x10
#define FAT_ATTR_ARCHIVE   0x20

// Time = (Hour<<11) + (Minute<<5) + (Second>>1)
#define FAT_MAKE_TIME(h, m, s) ((h<<11) + (m<<5) + (s>>1))
// Date = ((Year-1980)<<9) + (Month<<5) + Day
#define FAT_MAKE_DATE(y, m, d) (((y-1980)<<9) + (m<<5) + d)
// VFAT cannot handle file names longer than 256 characters but we need a spare bytes here
#define MAX_FILENAME_LEN 260 

#ifndef assert
#define assert(x) do { if(!(x)) { kprintf("VVFAT: assertion failed, file '%s', line %d.\n", __FILE__, __LINE__); while(1) Wait(SIGBREAKF_CTRL_C); } } while(0)
#endif
#define UNUSED __attribute__ ((__unused__))

/* /// */

/* /// "VFAT structures" */

#pragma pack(1)

struct BootSector
{
    UBYTE jump[3];
    UBYTE name[8];
    UWORD sector_size;
    UBYTE sectors_per_cluster;
    UWORD reserved_sectors;
    UBYTE number_of_fats;
    UWORD root_entries;
    UWORD total_sectors16;
    UBYTE media_type;
    UWORD sectors_per_fat;
    UWORD sectors_per_track;
    UWORD number_of_heads;
    ULONG hidden_sectors;
    ULONG total_sectors;
    union
    {
        struct
        {
            UBYTE drive_number;
            UBYTE reserved;
            UBYTE signature;
            ULONG id;
            UBYTE volume_label[11];
            UBYTE fat_type[8];
            UBYTE ignored[0x1c0];
        } fat16;
        struct
        {
            ULONG sectors_per_fat;
            UWORD flags;
            UBYTE major, minor;
            ULONG first_cluster_of_root_dir;
            UWORD info_sector;
            UWORD backup_boot_sector;
            UBYTE reserved1[12];
            UBYTE drive_number;
            UBYTE reserved2;
            UBYTE signature;
            ULONG id;
            UBYTE volume_label[11];
            UBYTE fat_type[8];
            UBYTE ignored[0x1a4];
        } fat32;
    } u;
    UBYTE magic[2];
};

struct CHS
{
    UBYTE head;
    UBYTE sector;
    UBYTE cylinder;
};

struct Partition
{
    UBYTE      attributes;
    struct CHS start_CHS;
    UBYTE      fs_type; 
    struct CHS end_CHS;
    ULONG      start_sector_long;
    ULONG      length_sector_long;
};

struct MBR
{
    UBYTE            ignored[0x1b8];
    ULONG            nt_id;
    UBYTE            ignored2[2];
    struct Partition partition[4];
    UBYTE            magic[2];
};

struct InfoSector
{
    ULONG signature1;
    UBYTE ignored[0x1e0];
    ULONG signature2;
    ULONG free_clusters;
    ULONG mra_cluster; // Most recently allocated cluster
    UBYTE reserved[14];
    UBYTE magic[2];
};

struct DirEntry
{
    UBYTE name[8];
    UBYTE extension[3];
    UBYTE attributes;
    UBYTE reserved[2];
    UWORD ctime;
    UWORD cdate;
    UWORD adate;
    UWORD begin_hi;
    UWORD mtime;
    UWORD mdate;
    UWORD begin;
    ULONG size;
};

#pragma pack()

/* /// */

/* /// "Management structures" */

// This structure are used to transparently access the files
#define MODE_UNDEFINED 0
#define MODE_FILE      1
#define MODE_DIRECTORY 2
#define MODE_DELETED   4

struct Mapping
{
    // begin is the first cluster, end is the last+1
    ULONG begin, end;
    // as directory is growable, no pointer may be used here
    ULONG dir_index;
    // the clusters of a file may be in any order; this points to the first
    LONG first_mapping_index;
    union
    {
        /* offset is
         * - the offset in the file (in clusters) for a file, or
         * - the next cluster of the directory for a directory, and
         * - the address of the buffer for a faked entry
         */
        struct
        {
            ULONG offset;
        } file;
        struct
        {
            LONG parent_mapping_index;
            LONG first_dir_index;
        } dir;
    } info;

    BPTR lock;
    UBYTE mode;
    BOOL read_only;
};

struct Array
{
    APTR  pointer;
    ULONG size;
    ULONG next;
    ULONG item_size;
};

struct VVFAT
{
    UWORD           spt;
    UWORD           heads;
    UWORD           cylinders;

    BPTR            dir_lock;

    ULONG           offset_to_bootsector;
    ULONG           offset_to_fat;
    ULONG           offset_to_root_dir;
    ULONG           offset_to_data;

    UBYTE           fat_type;

    UWORD           cluster_size;
    ULONG           cluster_count;  // Total number of clusters of this partition
    ULONG           first_cluster_of_root_dir;
    ULONG           max_fat_value;

    UBYTE           sectors_per_cluster;
    ULONG           sectors_per_fat;
    ULONG           sector_count;

    UWORD           root_entries;
    UWORD           reserved_sectors;

    struct Array    directory;
    struct Array    mapping;

    struct Mapping *current_mapping;
    UBYTE          *cluster;         // Points to current cluster
    ULONG           current_cluster;

    BPTR            current_fh;

    UBYTE          *cluster_buffer;  // Points to a buffer to hold temp data

    CONST_APTR      sector_cache_table;
    APTR            sector_cache_mempool;

    BOOL            modified;
    ULONG           temp_name_seed;
};

/* /// */

/* /// "Dynamic array management" */

static VOID arrayInit(struct Array *array, ULONG itemSize)
{
    array->pointer = NULL;

    array->size =
    array->next = 0;

    array->item_size = itemSize;
}

static VOID arrayFree(struct Array *array)
{
    FreeVec(array->pointer);
    array->pointer = NULL;

    array->size =
    array->next = 0;
}

// does not automatically grow
static APTR arrayGet(struct Array *array, ULONG index)
{
    assert(index < array->next);

    return (UBYTE *)array->pointer + index * array->item_size;
}

static BOOL arrayEnsureAllocated(struct Array *array, LONG index)
{
    if((index + 1) * array->item_size > array->size)
    {
        LONG newSize = array->size ? 2 * array->size : 32 * array->item_size;
        APTR pointer = AllocVec(newSize, MEMF_PUBLIC);

        if(pointer == NULL)
        {
            return FALSE;
        }

        memcpy(pointer, array->pointer, array->size);
        FreeVec(array->pointer);
        array->pointer = pointer;
        memset((UBYTE *)array->pointer + array->size, 0, newSize - array->size);
        array->size = newSize;
        array->next = index + 1;
    }
    return TRUE;
}

static APTR arrayGetNext(struct Array *array)
{
    ULONG index = array->next;

    if(!arrayEnsureAllocated(array, index))
    {
        return NULL;
    }

    array->next = index + 1;

    return arrayGet(array, index);
}

/* /// */

/* /// "Helper functions" */

static VOID setAttributes(CONST_STRPTR filename, ULONG attributes)
{
    // Note: on Amiga flags are protections not attributes!
    UBYTE protection = FIBF_EXECUTE;

    if(attributes & FAT_ATTR_READ_ONLY)
    {
        protection |= FIBF_WRITE | FIBF_DELETE;
    }
    if(attributes & FAT_ATTR_HIDDEN)
    {
        // Hidden files do not exist on Amiga,
        // but this flag is commonly used for this purpose
        protection |= FIBF_HOLD;
    }
    if(attributes & FAT_ATTR_ARCHIVE)
    {
        protection &= ~FIBF_ARCHIVE;
    }
    if(attributes & FAT_ATTR_SYSTEM)
    {
        protection |= FIBF_PURE;
    }
    //if(attributes & DIR_ATTR_VOLUME_ID)
    //if(attributes & DIR_ATTR_DIRECTORY)

    SetProtection(filename, protection);
}

static BOOL deleteAll(CONST_STRPTR filename)
{
    BOOL success = TRUE;
    struct FileInfoBlock *fib = (struct FileInfoBlock*)AllocDosObject(DOS_FIB, NULL);

    if(fib)
    {
        BPTR lock = Lock(filename, SHARED_LOCK);

        if(lock)
        {
            if(Examine(lock, fib))
            {
                // Directory?
                if(fib->fib_DirEntryType > 0)
                {
                    while(ExNext(lock, fib))
                    {
                        //Directory?
                        if(fib->fib_DirEntryType > 0)
                        {
                            if(!deleteAll(fib->fib_FileName))
                            {
                                success = FALSE;
                            }
                        }
                        else
                        {
                            SetProtection(fib->fib_FileName, 0);
                            if(!DeleteFile(fib->fib_FileName))
                            {
                                success = FALSE;
                            }
                        }
                    }
                }
            }
            UnLock(lock);
        }
        FreeDosObject(DOS_FIB, fib);

        SetProtection(filename, 0);
        if(!DeleteFile(filename))
        {
            success = FALSE;
        }
    }

    return success;
}

static STRPTR getNameFromLock(BPTR lock)
{
    ULONG size = 128;
    STRPTR name;

    while((name = (STRPTR)AllocVec(size, MEMF_PUBLIC)))
    {
        if(NameFromLock(lock, name, size))
        {
            break;
        }

        FreeVec(name);

        if(IoErr() == ERROR_LINE_TOO_LONG)
        {
            size *= 2;
        }
    }

    return name;
}

static STRPTR createTempName(CONST_STRPTR name, ULONG *seed)
{
    const UBYTE nibbles[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    ULONG len = strlen(name);
    STRPTR temp = (STRPTR)AllocVec(len + 9 + 1, MEMF_PUBLIC);

    if(temp)
    {
        ULONG i;

        memcpy(temp, name, len);
        temp[len] = '_';

        for(i=1; i<=8; i++)
        {
            temp[len + i] = nibbles[(*seed >> (32 - (i * 4)) & 0xf)];
        }
        temp[len + i] = '\0';
        (*seed)++;
    }

    return temp;
}

// dest is assumed to hold 258 bytes, and pads with 0xffff up to next multiple of 26
static int shortToLongName(STRPTR dest, CONST_STRPTR src)
{
    LONG i;
    LONG len;

    for(i=0; i<129 && src[i]; i++)
    {
        dest[2*i] = src[i];
        dest[2*i+1] = 0;
    }
    len = 2 * i;
    dest[2*i] = dest[2*i+1] = 0;

    for(i=2*i+2; i%26; i++)
    {
        dest[i] = 0xff;
    }

    return len;
}

static char isLongName(const struct DirEntry *direntry)
{
    return direntry->attributes == 0xf;
}

static void setBeginOfDirEntry(struct DirEntry *direntry, ULONG begin)
{
    direntry->begin = BE_SWAPWORD(begin & 0xffff);
    direntry->begin_hi = BE_SWAPWORD((begin >> 16) & 0xffff);
}

static BOOL isSameEntry(struct DirEntry *entry1, struct DirEntry *entry2)
{
    if((entry1->begin != entry2->begin)
    || (entry1->begin_hi != entry2->begin_hi)
    || (entry1->size != entry2->size)
    || (entry1->mdate != entry2->mdate)
    || (entry1->mtime != entry2->mtime))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

static UBYTE fatCheckSum(const struct DirEntry *entry)
{
    UBYTE chksum = 0;
    LONG i;

    for(i=0; i<11; i++)
    {
        UBYTE c;

        c = (i < 8) ? entry->name[i] : entry->extension[i-8];
        chksum = (((chksum & 0xfe) >> 1) | ((chksum & 0x01) ? 0x80:0)) + c;
    }

    return chksum;
}

#ifdef __HAIKU__
static UWORD fatDate(const struct tm *time)
{
    return BE_SWAPWORD(FAT_MAKE_DATE(time->tm_year, time->tm_mon, time->tm_mday));
}
#else
static UWORD fatDate(const struct DateStamp *time)
{
    struct ClockData c;
    ULONG s = (time->ds_Days * 24 * 60 + time->ds_Minute) * 60 + time->ds_Tick / 50;

    Amiga2Date(s, &c);

    return BE_SWAPWORD(FAT_MAKE_DATE(c.year, c.month, c.mday));
}
#endif

#ifdef __HAIKU__
static UWORD fatTime(const struct tm *time)
{
    return BE_SWAPWORD(FAT_MAKE_TIME(time->tm_hour, time->tm_min, time->tm_sec));
}
#else
static UWORD fatTime(const struct DateStamp *time)
{
    UBYTE h = time->ds_Minute / 60;
    UBYTE m = time->ds_Minute % 60;
    UBYTE s = time->ds_Tick / 50;

    return BE_SWAPWORD(FAT_MAKE_TIME(h, m, s));
}
#endif

/* /// */

/* /// "Misc. private functions" */

static UQUAD cluster2Sector(struct VVFAT *v, ULONG clusterNum)
{
    return ((UQUAD)(clusterNum - 2) * v->sectors_per_cluster) + v->offset_to_data;
}

static BOOL sector2CHS(struct VVFAT *v, ULONG spos, struct CHS *chs)
{
    BOOL isLBA;
    ULONG head;
    ULONG sector;

    sector = spos % v->spt;
    spos   /= v->spt;
    head   = spos % v->heads;
    spos   /= v->heads;

    if(spos > 1023)
    {
        /* Overflow,
        it happens if 32bit sector positions are used, while CHS is only 24bit.
        Windows/Dos is said to take 1023/255/63 as nonrepresentable CHS */
        chs->head     = 0xff;
        chs->sector   = 0xff;
        chs->cylinder = 0xff;

        isLBA = TRUE;
    }
    else
    {
        chs->head     = (UBYTE)head;
        chs->sector   = (UBYTE)((sector+1) | ((spos >> 8) << 6));
        chs->cylinder = (UBYTE)spos;

        isLBA = FALSE;
    }

    return isLBA;
}

static APTR fatAddress(struct VVFAT *v, ULONG cluster)
{
    ULONG address;
    ULONG sectorNum;
    ULONG offsetAdr;
    UBYTE *fatSector;

    if(v->fat_type == 12)
    {
        address = cluster * 3 / 2;
    }
    else if(v->fat_type == 16)
    {
        address = cluster * 2;
    }
    else /* if(v->fat_type == 32) */
    {
        address = cluster * 4;
    }

    sectorNum = address / 0x200;
    offsetAdr = address % 0x200;

    if(!GetHashDataByKey(v->sector_cache_table, v->offset_to_fat + sectorNum, (APTR *)&fatSector))
    {
        fatSector = (UBYTE*)AllocPooled(v->sector_cache_mempool, 0x200);

        if(fatSector)
        {
            memset(fatSector, 0, 0x200);
            InsertHash(v->sector_cache_table, v->offset_to_fat + sectorNum, fatSector);
        }
    }

    if(fatSector)
    {
        return &fatSector[offsetAdr];
    }
    else
    {
        return NULL;
    }
}

static BOOL fatCreateSpare(struct VVFAT *v)
{
    ULONG fatNum;
    ULONG sectorNum;
    APTR fatSector;
    APTR spareSector;

    for(fatNum=1; fatNum<NUMBER_OF_FAT; fatNum++)
    {
        for(sectorNum=0; sectorNum<v->sectors_per_fat; sectorNum++)
        {
            if(GetHashDataByKey(v->sector_cache_table, v->offset_to_fat + sectorNum, (APTR *)&fatSector))
            {
                spareSector = AllocPooled(v->sector_cache_mempool, 0x200);

                if(spareSector)
                {
                    memcpy(spareSector, fatSector, 0x200);
                    InsertHash(v->sector_cache_table, v->offset_to_fat + (fatNum * v->sectors_per_fat) + sectorNum, spareSector);
                }
                else
                {
                    return FALSE;
                }
            }
        }
    }
    return TRUE;
}

static BOOL fatSet(struct VVFAT *v, ULONG cluster, ULONG value)
{
    APTR address = fatAddress(v, cluster);

    if(address)
    {
        if(v->fat_type == 12)
        {
            UBYTE *p = (UBYTE *)address;

            if((cluster & 1) == 0)
            {
                p[0] = value & 0xff;
                p[1] = (p[1] & 0xf0) | ((value >> 8) & 0xf);
            }
            else
            {
                p[0] = (p[0] & 0xf) | ((value & 0xf) << 4);
                p[1] = (value >> 4);
            }
        }
        else if(v->fat_type == 16)
        {
            UWORD *entry = (UWORD *)address;
            *entry = BE_SWAPWORD(value & 0xffff);
        }
        else /* if(v->fat_type == 32) */
        {
            ULONG *entry = (ULONG *)address;
            *entry = BE_SWAPLONG(value);
        }

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static ULONG fatGet(struct VVFAT *v, ULONG cluster)
{
    APTR address = fatAddress(v, cluster);

    if(address)
    {
        if(v->fat_type == 12)
        {
            UBYTE *p = (UBYTE*)address;

            if((cluster & 1) == 0)
            {
                return p[0] | ((p[1] & 0x0f) << 8);
            }
            else
            {
                return (p[0] >> 4) | (p[1] << 4);
            }
        }
        else if(v->fat_type == 16)
        {
            return BE_SWAPWORD(*(UWORD *)address);
        }
        else /* if(v->fat_type == 32) */
        {
            return BE_SWAPLONG(*(ULONG *)address);
        }
    }
    else
    {
        return 0;
    }
}

static struct DirEntry * createLongFileName(struct VVFAT *v, CONST_STRPTR fileName)
{
    char buffer[MAX_FILENAME_LEN];

    LONG length = shortToLongName(buffer, fileName);
    LONG numberOfEntries = (length + 25) / 26;
    LONG i;
    struct DirEntry *entry;

    for(i=0; i<numberOfEntries; i++)
    {
        entry = (struct DirEntry *)arrayGetNext(&v->directory);
        entry->attributes = 0xf;
        entry->reserved[0] = 0;
        entry->begin = 0;
        entry->name[0] = (numberOfEntries - i) | (i==0 ? 0x40:0);
    }

    for(i=0; i<26*numberOfEntries; i++)
    {
        LONG offset = (i % 26);

        if(offset < 10)
        {
            offset = 1 + offset;
        }
        else if(offset < 22)
        {
            offset = 14 + offset - 10;
        }
        else
        {
            offset = 28 + offset - 22;
        }

        entry = (struct DirEntry *)arrayGet(&v->directory, v->directory.next - 1 - (i / 26));
        entry->name[offset] = buffer[i];
    }
    return (struct DirEntry *)arrayGet(&v->directory, v->directory.next-numberOfEntries);
}

static struct DirEntry * createDotName(struct VVFAT *v, CONST_STRPTR fileName)
{
    struct DirEntry *entry = (struct DirEntry *)arrayGetNext(&v->directory);
    memset(entry->name, 0x20, 11);
    memcpy(entry->name, fileName, strlen(fileName));
    return entry;
}

static struct DirEntry * createShortAndLongName(struct VVFAT *v, ULONG directoryStart, CONST_STRPTR fileName)
{
    LONG i, j;
    LONG longIndex = v->directory.next;
    struct DirEntry *entry = NULL;
    struct DirEntry *entryLong = NULL;

    entryLong = createLongFileName(v, fileName);

    i = strlen(fileName);

    for(j=i-1; j>0 && fileName[j]!='.'; j--);

    if(j > 0)
    {
        i = (j > 8 ? 8 : j);
    }
    else if(i > 8)
    {
        i = 8;
    }

    entry = (struct DirEntry *)arrayGetNext(&v->directory);
    memset(entry->name, 0x20, 11);
    memcpy(entry->name, fileName, i);

    if(j > 0)
    {
        for(i=0; i<3 && fileName[j+1+i]; i++)
        {
            entry->extension[i] = fileName[j+1+i];
        }
    }

    // Upcase & remove unwanted characters
    for(i=10; i>=0; i--)
    {
        if(i == 10 || i == 7)
        {
            for (; i>0 && entry->name[i]==' '; i--);
        }
        if(entry->name[i] <= ' ' || entry->name[i] > 0x7e
        || strchr(FAT_EXCLUDE_CHAR, entry->name[i]))
        {
            entry->name[i] = FAT_REPLACE_CHAR;
        }
        else if(entry->name[i] >= 'a' && entry->name[i] <= 'z')
        {
            entry->name[i] += 'A'-'a';
        }
    }
    if(entry->name[0] == 0xe5) entry->name[0] = 0x05;

    // Mangle duplicates
    while(1)
    {
        struct DirEntry *entry1 = (struct DirEntry *)arrayGet(&v->directory, directoryStart);
        LONG j;

        for(; entry1<entry; entry1++)
        {
            if(!isLongName(entry1) && memcmp(entry1->name, entry->name, 11) == 0)
            {
                break; // found dupe
            }
        }
        if(entry1 == entry) // no dupe found
        {
            break;
        }

        // Use all 8 characters of name
        if(entry->name[7]==' ')
        {
            int j;

            for(j=6; j>0 && entry->name[j]==' '; j--)
            {
                entry->name[j] = '~';
            }
        }

        // Increment number
        for(j=7; j>0 && entry->name[j]=='9'; j--)
        {
            entry->name[j] = '0';
        }

        if(j > 0)
        {
            if(entry->name[j] < '0' || entry->name[j] > '9')
            {
                entry->name[j] = '0';
            }
            else
            {
                entry->name[j]++;
            }
        }
    }

    // Calculate checksum; propagate to long name
    if(entryLong)
    {
        UBYTE chksum = fatCheckSum(entry);

        // Calculate a new, because realloc could have taken place
        entryLong = (struct DirEntry *)arrayGet(&v->directory, longIndex);
        while(entryLong<entry && isLongName(entryLong))
        {
            entryLong->reserved[1] = chksum;
            entryLong++;
        }
    }

    return entry;
}

static VOID closeCurrentFile(struct VVFAT *v)
{
    if(v->current_mapping)
    {
        v->current_mapping = NULL;

        Close(v->current_fh);
        v->current_fh = (BPTR)0;
    }
    v->current_cluster = (ULONG)-1;
}

static BOOL openFile(struct VVFAT *v, struct Mapping *mapping)
{
    if(!mapping)
    {
        return FALSE;
    }

    if(!v->current_mapping || SameLock(v->current_mapping->lock, mapping->lock) != LOCK_SAME)
    {
        //{ STRPTR buffer = getNameFromLock(mapping->lock); kprintf("Accessing mapping of %s (%p): [%p]%s -> [%ld,%ld]\n", buffer ? buffer : "???", mapping, mapping->lock, mapping->read_only ? "*" : "", mapping->begin, mapping->end); FreeVec(buffer) }

        // Open file
        BPTR lock = DupLock(mapping->lock);
        BPTR fh = OpenFromLock(lock);

        if(fh == (BPTR)0)
        {
            UnLock(lock);
            return FALSE;
        }

        closeCurrentFile(v);

        v->current_fh = fh;
        v->current_mapping = mapping;
    }

    return TRUE;
}

// This function checks if lock points to the same object that an existing
// mapping->lock, and if so, it renames the associated object to a temporary
// name to avoid eventual overwrite by another object with the same name.
// Since the mappings are sorted by cluster, this is expensive: O(n).
static VOID checkMappingForLock(struct VVFAT *v, BPTR lock)
{
    ULONG i;

    for(i=0; i<v->mapping.next; i++)
    {
        struct Mapping *mapping = (struct Mapping *)arrayGet(&v->mapping, i);

        if((mapping->first_mapping_index < 0) && SameLock(lock, mapping->lock) == LOCK_SAME)
        {
            STRPTR originalPath = getNameFromLock(lock);

            if(originalPath)
            {
                STRPTR tempName = createTempName(FilePart(originalPath), &v->temp_name_seed);

                if(tempName)
                {
                    if(!Rename(FilePart(originalPath), tempName))
                    {
                        kprintf("VVFAT: error %ld when renaming %s to %s.\n", IoErr(), FilePart(originalPath), tempName);
                    }
                    else
                    {
                        //kprintf("checkMappingForLock: mapped object renamed from %s to %s to avoid overwrite by a new object.\n", FilePart(originalPath), tempName);
                    }
                    FreeVec(tempName);
                }
                else
                {
                    kprintf("VVFAT: not enough memory to rename an object.\n");
                }
                FreeVec(originalPath);
            }
            else
            {
                kprintf("VVFAT: not enough memory to rename an object.\n");
            }
            break;
        }
    }
}

// mappings between index1 and index2-1 are supposed to be ordered
// return value is the index of the last mapping for which end>cluster_num
static LONG findMappingForClusterAux(struct VVFAT *v, ULONG clusterNum, ULONG index1, ULONG index2)
{
    while(1)
    {
        ULONG index3;
        struct Mapping *mapping;

        index3 = (index1 + index2) / 2;
        mapping = (struct Mapping *)arrayGet(&v->mapping, index3);

        assert(mapping->begin < mapping->end);

        if(mapping->begin >= clusterNum)
        {
            assert(index2 != index3 || index2 == 0);

            if(index2 == index3)
            {
                return index1;
            }
            index2 = index3;
        }
        else
        {
            if(index1 == index3)
            {
                return (mapping->end <= clusterNum) ? index2 : index1;
            }
            index1 = index3;
        }

        assert(index1 <= index2);
    }
}

static struct Mapping * findMappingForCluster(struct VVFAT *v, ULONG clusterNum)
{
    ULONG index = findMappingForClusterAux(v, clusterNum, 0, v->mapping.next);
    struct Mapping *mapping;

    if(index >= v->mapping.next)
    {
        return NULL;
    }

    mapping = (struct Mapping *)arrayGet(&v->mapping, index);

    if(mapping->begin > clusterNum)
    {
        return NULL;
    }

    assert(mapping->begin <= clusterNum && mapping->end > clusterNum);

    return mapping;
}

static BOOL readCluster(struct VVFAT *v, ULONG clusterNum)
{
    struct Mapping *mapping;

    if(v->current_cluster != clusterNum)
    {
        LONG offset;

        assert(!v->current_mapping || v->current_fh || (v->current_mapping->mode & MODE_DIRECTORY));

        if(!v->current_mapping
        || v->current_mapping->begin > clusterNum
        || v->current_mapping->end <= clusterNum)
        {
            // Binary search of mappings for file
            mapping = findMappingForCluster(v, clusterNum);

            assert(!mapping || (clusterNum >= mapping->begin && clusterNum < mapping->end));

            if(mapping && mapping->mode & MODE_DIRECTORY)
            {
                closeCurrentFile(v);
                v->current_mapping = mapping;
read_cluster_directory:
                offset = v->cluster_size * (clusterNum - v->current_mapping->begin);
                v->cluster = (UBYTE *)v->directory.pointer+offset + 0x20 * v->current_mapping->info.dir.first_dir_index;

                assert(((v->cluster - (UBYTE *)v->directory.pointer) % v->cluster_size) == 0);
                assert((UBYTE *)v->cluster + v->cluster_size <= (UBYTE *)v->directory.pointer + v->directory.next * v->directory.item_size);

                v->current_cluster = clusterNum;

                return TRUE;
            }

            if(!openFile(v, mapping))
            {
                return FALSE;
            }
        }
        else if(v->current_mapping->mode & MODE_DIRECTORY)
        {
            goto read_cluster_directory;
        }

        assert(v->current_fh);

        offset = v->cluster_size * (clusterNum - v->current_mapping->begin) + v->current_mapping->info.file.offset;

        if(Seek(v->current_fh, offset, OFFSET_BEGINNING) < 0)
        {
            return FALSE;
        }

        v->cluster = v->cluster_buffer;

        if(Read(v->current_fh, v->cluster, v->cluster_size) < 0)
        {
            v->current_cluster = (ULONG)-1;
            return FALSE;
        }
        v->current_cluster = clusterNum;
    }

    return TRUE;
}

/* /// */

/* /// "Local directory synchronization" */

struct DirEntry * readDirEntry(UBYTE *buffer, TEXT filename[MAX_FILENAME_LEN])
{
    const UBYTE lfnMap[13] = {1, 3, 5, 7, 9, 14, 16, 18, 20, 22, 24, 28, 30};
    struct DirEntry *entry;
    BOOL lfnFound = FALSE;
    TEXT lfnTemp[MAX_FILENAME_LEN];
    ULONG i;

    filename[0] =
    lfnTemp[0] = '\0';

    while(1)
    {
        entry = (struct DirEntry *)buffer;

        /*kprintf("** readDirEntry: %02lx - %02lx%02lx%02lx%02lx%02lx%02lx%02lx%02lx %02lx%02lx%02lx '%c%c%c%c%c%c%c%c'.'%c%c%c'\n",
            entry->attributes,
            entry->name[0], entry->name[1], entry->name[2], entry->name[3], entry->name[4], entry->name[5], entry->name[6], entry->name[7],
            entry->extension[0], entry->extension[1], entry->extension[2],
            entry->name[0], entry->name[1], entry->name[2], entry->name[3], entry->name[4], entry->name[5], entry->name[6], entry->name[7],
            entry->extension[0], entry->extension[1], entry->extension[2]);*/

        // End of entries?
        if(entry->name[0] == '\0')
        {
            return NULL;
            break;
        }
        // Deleted entry?
        else if(entry->name[0] == 0xe5)
        {
            lfnFound = FALSE;
            filename[0] =
            lfnTemp[0] = '\0';
            buffer += 32;
        }
        // Actual entry?
        else if(entry->name[0] != '.' && (entry->attributes & 0x0f) != 0x08)
        {
            if(isLongName(entry))
            {
                for(i=0; i<13; i++)
                {
                    lfnTemp[i] = buffer[lfnMap[i]];
                }
                lfnTemp[i] = '\0';
                strcat(lfnTemp, filename);
                strcpy(filename, lfnTemp);
                lfnFound = TRUE;
                buffer += 32;
            }
            else
            {
                if(!lfnFound)
                {
                    if(entry->name[0] == 0x05)
                    {
                        entry->name[0] = 0xe5;
                    }
                    memcpy(filename, entry->name, 8);
                    filename[8] = '\0';
                    i = 8 - 1;
                    while((i > 0) && (filename[i] == ' '))
                    {
                        filename[i--] = '\0';
                    }
                    if(entry->extension[0] != ' ')
                    {
                        strcat(filename, ".");
                    }
                    memcpy(filename+i+2, entry->extension, 3);
                    filename[i+2+3] = '\0';
                    i += 2 + 3 - 1;
                    while(filename[i] == ' ')
                    {
                        filename[i--] = '\0';
                    }
#ifdef FORCE_SHORT_NAME_TO_LOWER_CASE
                    for(i=0; i<strlen(filename); i++)
                    {
                        if(filename[i] > 0x40 && filename[i] < 0x5b)
                        {
                            filename[i] |= 0x20;
                        }
                    }
#endif
                }
                return entry;
            }
        }
        else
        {
            buffer += 32;
        }
    }
}


static BOOL writeFile(struct VVFAT *v, CONST_STRPTR filename, struct DirEntry *entry)
{
    BPTR fh;
    ULONG csize, fsize, fstart, cur, next, rsvdClusters, badCluster;
    UQUAD offset;
    UBYTE *buffer;
    struct DateStamp ds;

    csize = v->sectors_per_cluster * 0x200;
    rsvdClusters = v->max_fat_value - 15;
    badCluster = v->max_fat_value - 8;
    fsize = BE_SWAPLONG(entry->size);
    fstart = BE_SWAPWORD(entry->begin) | (BE_SWAPWORD(entry->begin_hi) << 16);

    // Ensure we will have full access during update
    SetProtection(filename, 0);

    fh = Open(filename, MODE_NEWFILE);

    if(fh == (BPTR)0)
    {
        kprintf("Failed to create file '%s': %d\n", filename, IoErr());
        return FALSE;
    }

    buffer = (UBYTE*)AllocVec(csize, MEMF_PUBLIC);

    next = fstart;

    do
    {
        cur = next;
        offset = cluster2Sector(v, cur);

        VVFAT_ReadData(v, buffer, offset * 0x200, csize);

        if(fsize > csize)
        {
            Write(fh, buffer, csize);
            fsize -= csize;
        }
        else
        {
            Write(fh, buffer, fsize);
        }

        next = fatGet(v, cur);

        if((next >= rsvdClusters) && (next < badCluster))
        {
            kprintf("VVFAT: reserved clusters not supported.\n");
        }
    }
    while(next < rsvdClusters);

    Close(fh);

    SetFileDate(filename, DateStamp(&ds));

    return TRUE;
}

static VOID parseDirectory(struct VVFAT *v, BPTR dirLock, ULONG startCluster)
{
    ULONG csize, fstart, cur, next, size, rsvdClusters;
    UQUAD offset;
    TEXT filename[MAX_FILENAME_LEN];
    BPTR oldDir;
    UBYTE *buffer, *ptr;
    struct DirEntry *entry, *newentry;
    struct Mapping *mapping;

    csize = v->sectors_per_cluster * 0x200;

    rsvdClusters = v->max_fat_value - 15;

    if(startCluster == 0)
    {
        size = v->root_entries * 32;
        offset = v->offset_to_root_dir;
        buffer = (UBYTE*)AllocVec(size, MEMF_PUBLIC);
        if(buffer == NULL)
        {
            kprintf("VVFAT: out of memory during parseDirectory().\n");
            return;
        }
        VVFAT_ReadData(v, buffer, offset * 0x200, size);
    }
    else
    {
        size = csize;
        buffer = (UBYTE*)AllocVec(size, MEMF_PUBLIC);
        if(buffer == NULL)
        {
            kprintf("VVFAT: out of memory during parseDirectory().\n");
            return;
        }
        next = startCluster;
        do
        {
            cur = next;
            offset = cluster2Sector(v, cur);
            VVFAT_ReadData(v, buffer+(size-csize), offset * 0x200, csize);
            next = fatGet(v, cur);

            if(next < rsvdClusters)
            {
                APTR newBuffer = AllocVec(size + csize, MEMF_PUBLIC);
                if(newBuffer == NULL)
                {
                    FreeVec(buffer);
                    kprintf("VVFAT: out of memory during parseDirectory().\n");
                    return;
                }
                memcpy(newBuffer, buffer, size);
                FreeVec(buffer);
                size += csize;
                buffer = (UBYTE*)newBuffer;
            }
        }
        while(next < rsvdClusters);
    }

    ptr = buffer;

    oldDir = CurrentDir(dirLock);

    do
    {
        newentry = readDirEntry(ptr, filename);

        if(newentry != NULL)
        {
            BPTR lock = Lock(filename, SHARED_LOCK);

            fstart = BE_SWAPWORD(newentry->begin) | (BE_SWAPWORD(newentry->begin_hi) << 16);

            mapping = findMappingForCluster(v, fstart);

            if(mapping == NULL)
            {
                //kprintf("Sync: no mapping found for object %s from %p!\n", filename, dirLock);

                if(lock)
                {
                    checkMappingForLock(v, lock);
                    UnLock(lock);
                }

                if(newentry->attributes & FAT_ATTR_DIRECTORY)
                {
                    //kprintf("    Sync: parsing a new directory...\n");
                    BPTR subDirLock = CreateDir(filename);
                    parseDirectory(v, subDirLock, fstart);
                    UnLock(subDirLock);
                }
                else
                {
                    //kprintf("    Sync: creating a new file.\n");
                    writeFile(v, filename, newentry);
                }
            }
            else
            {
                mapping->mode &= ~MODE_DELETED;

                //kprintf("Sync: actual mapping found for object %s from %p!\n", filename, dirLock);
                entry = (struct DirEntry *)arrayGet(&v->directory, mapping->dir_index);

                if(lock)
                {
                    if(SameLock(lock, mapping->lock) == LOCK_SAME)
                    {
                        UnLock(lock);

                        //kprintf("  Sync: entry maps to the same object.\n");

                        if(newentry->attributes & FAT_ATTR_DIRECTORY)
                        {
                            //kprintf("    Sync: object is a directory to parse...\n");
                            parseDirectory(v, mapping->lock, fstart);
                        }
                        else
                        {
                            if(!isSameEntry(entry, newentry))
                            {
                                //kprintf("    Sync: object '%s' is a file which needs to be updated (1).\n", filename);
                                UnLock(mapping->lock);
                                writeFile(v, filename, newentry);
                                mapping->lock = Lock(filename, SHARED_LOCK);
                            }
                            else
                            {
                                //kprintf("    Sync: object is an unchanged file; nothing to do.\n");
                            }
                        }
                    }
                    else
                    {
                        checkMappingForLock(v, lock);
                        UnLock(lock);

                        //kprintf("  Sync: entry maps to a different object object; delete it.\n");
                        deleteAll(filename);

                        if(newentry->attributes & FAT_ATTR_DIRECTORY)
                        {
                            BPTR subDirLock = CreateDir(filename);
                            //kprintf("    Sync: this is a directory; we create it and parse its contents.\n");
                            parseDirectory(v, subDirLock, fstart);
                            UnLock(subDirLock);
                        }
                        else
                        {
                            //kprintf("    Sync: this is a new file; we create it.\n");
                            writeFile(v, filename, newentry);
                        }
                    }
                }
                else
                {
                    STRPTR originalPath = getNameFromLock(mapping->lock);

                    if(originalPath)
                    {
                        if(Stricmp(FilePart(originalPath), filename) != 0)
                        {
                            //kprintf("  Sync: object was renamed from %s to %s.\n", FilePart(originalPath), filename);
                            if(!Rename(FilePart(originalPath), filename))
                            {
                                //kprintf("VVFAT: error %ld when renaming %s to %s.\n", IoErr(), FilePart(originalPath), filename);
                            }
                        }
                        FreeVec(originalPath);
                    }
                    else
                    {
                        kprintf("VVFAT: out of memory to compose a file name before renaming.\n");
                    }

                    if(newentry->attributes & FAT_ATTR_DIRECTORY)
                    {
                        //kprintf("    Sync: object is a directory to parse...\n");
                        parseDirectory(v, mapping->lock, fstart);
                    }
                    else
                    {
                        if(!isSameEntry(entry, newentry))
                        {
                            kprintf("    Sync: object '%s' is a file which needs to be updated (2).\n", filename);
                            writeFile(v, filename, newentry);
                        }
                        else
                        {
                            //kprintf("    Sync: object is an unchanged file; nothing to do.\n");
                        }
                    }
                }
            }

            setAttributes(filename, newentry->attributes);

            ptr = (UBYTE *)newentry + 32;
        }
    }
    while((newentry != NULL) && ((ULONG)(ptr - buffer) < size));

    CurrentDir(oldDir);

    FreeVec(buffer);
}


static VOID synchronize(struct VVFAT *v)
{
    struct Mapping *mapping;
    ULONG i;

    v->temp_name_seed = 0xdeadbabe;

    // Mark all mapped directories & files for delete
    for(i=1; i<v->mapping.next; i++)
    {
        mapping = (struct Mapping *)arrayGet(&v->mapping, i);

        if(mapping->first_mapping_index < 0)
        {
            mapping->mode |= MODE_DELETED;
        }
    }

    // Parse new directory tree and create / modify directories and files
    parseDirectory(v, v->dir_lock, (v->fat_type == 32) ? v->first_cluster_of_root_dir : 0);

    // Remove all directories and files still marked for delete
    for(i=v->mapping.next-1; i>0; i--)
    {
        mapping = (struct Mapping *)arrayGet(&v->mapping, i);

        if(mapping->mode & MODE_DELETED)
        {
            if(mapping->lock)
            {
                STRPTR path = getNameFromLock(mapping->lock);

                if(path)
                {
                    UnLock(mapping->lock);
                    mapping->lock = (BPTR)0;
                    deleteAll(path);
                    FreeVec(path);
                }
                else
                {
                    kprintf("VVFAT: out of memory to compose a file name before deleting.\n");
                }
            }
        }
    }
}

/* /// */

/* /// "Local directory reading" */

static VOID feedVolumeLabel(struct VVFAT *v, struct DirEntry *entry)
{
    struct FileInfoBlock *fib = (struct FileInfoBlock*)AllocDosObject(DOS_FIB, NULL);

    entry->attributes = FAT_ATTR_ARCHIVE | FAT_ATTR_VOLUME_ID;
    memcpy(entry->name, FAT_VOLUME_LABEL, 11);

    if(fib && Examine(v->dir_lock, fib))
    {
        entry->ctime =
        entry->mtime = fatTime(&fib->fib_Date);
        entry->cdate =
        entry->adate =
        entry->mdate = fatDate(&fib->fib_Date);

        FreeDosObject(DOS_FIB, fib);
    }
    else
    {
        entry->ctime =
        entry->mtime = FAT_MAKE_TIME(12,00,00);
        entry->cdate =
        entry->adate =
        entry->mdate = FAT_MAKE_DATE(2022,01,01);
    }
}

static VOID feedDirEntry(struct VVFAT *v, ULONG mappingIndex, const struct FileInfoBlock *fib, struct DirEntry *entry)
{
    entry->attributes = 0;

    // Note: on Amiga flags are protections not attributes!
    if(fib->fib_DirEntryType > 0)
    {
        entry->attributes |= FAT_ATTR_DIRECTORY;
    }
    if((fib->fib_Protection & (FIBF_WRITE | FIBF_DELETE)) != 0)
    {
        entry->attributes |= FAT_ATTR_READ_ONLY;
    }
    if((fib->fib_Protection & FIBF_ARCHIVE) == 0)
    {
        entry->attributes |= FAT_ATTR_ARCHIVE;
    }
    if((fib->fib_Protection & FIBF_HOLD) != 0)
    {
        // Hidden files do not exist on Amiga,
        // but this flag is commonly used for this purpose
        entry->attributes |= FAT_ATTR_HIDDEN;
    }
    if((fib->fib_Protection & FIBF_PURE) != 0)
    {
        // Pure flags is not exactly the same as System
        // but it sounds good to use it this way
        entry->attributes |= FAT_ATTR_SYSTEM;
    }
    //if(fib->fib_Protection & FIBF_SCRIPT)
    //if(fib->fib_Protection & FIBF_EXECUTE)
    //if(fib->fib_Protection & FIBF_READ)

    entry->reserved[0] =
    entry->reserved[1] =0;

    entry->ctime = fatTime(&fib->fib_Date);
    entry->cdate = fatDate(&fib->fib_Date);

    entry->adate = fatDate(&fib->fib_Date);

    entry->begin_hi = 0;

    entry->mtime = fatTime(&fib->fib_Date);
    entry->mdate = fatDate(&fib->fib_Date);

    entry->begin = 0; // do that later

    if(fib->fib_Size > 0x7fffffff)
    {
        kprintf("VVFAT: file '%s' is larger than 2GB\n", fib->fib_FileName);
    }

    entry->size = BE_SWAPLONG(fib->fib_Size);

    // create mapping for this entry
    if(fib->fib_DirEntryType > 0 || fib->fib_Size)
    {
        v->current_mapping = (struct Mapping *)arrayGetNext(&v->mapping);
        v->current_mapping->begin = 0;
        v->current_mapping->end = fib->fib_Size;

        /*
        * we get the direntry of the most recent direntry, which
        * contains the short name and all the relevant information.
        */
        v->current_mapping->dir_index = v->directory.next-1;
        v->current_mapping->first_mapping_index = -1;

        if(fib->fib_DirEntryType > 0)
        {
            v->current_mapping->mode = MODE_DIRECTORY;
            v->current_mapping->info.dir.parent_mapping_index = mappingIndex;
        }
        else
        {
            v->current_mapping->mode = MODE_UNDEFINED;
            v->current_mapping->info.file.offset = 0;
        }

        v->current_mapping->lock = Lock(fib->fib_FileName, SHARED_LOCK);
        v->current_mapping->read_only = (entry->attributes & FAT_ATTR_READ_ONLY) != 0;

        assert(v->current_mapping->lock != (BPTR)0);
    }
}

/*
 * Read a directory. (the index of the corresponding mapping must be passed).
 */
static BOOL readDirectory(struct VVFAT *v, ULONG mappingIndex)
{
    struct Mapping *mapping = (struct Mapping *)arrayGet(&v->mapping, mappingIndex);
    LONG parentIndex = mapping->info.dir.parent_mapping_index;
    struct Mapping *parentMapping = (struct Mapping *)(parentIndex >= 0 ? arrayGet(&v->mapping, parentIndex) : NULL);
    LONG firstClusterOfParent = parentMapping ? (LONG)parentMapping->begin : -1;
    ULONG firstCluster = mapping->begin;
    struct FileInfoBlock *fib;
    struct DirEntry *dirEntry;
    int count = 0;
    LONG i;

    assert(mapping->mode & MODE_DIRECTORY);

    //{ STRPTR buffer = getNameFromLock(mapping->lock); kprintf("VVFAT_Create: readDirectory(%s:%p)\n", buffer ? buffer : "???", mapping->lock); FreeVec(buffer); }

    i =
    mapping->info.dir.first_dir_index = firstCluster == v->first_cluster_of_root_dir ? 0 : v->directory.next;

    if(firstCluster != v->first_cluster_of_root_dir)
    {
        // Create the top entries of a subdirectory
        dirEntry = createDotName(v, ".");
        setBeginOfDirEntry(dirEntry, firstCluster);
        dirEntry = createDotName(v, "..");
        setBeginOfDirEntry(dirEntry, firstClusterOfParent);
    }

    if((fib = (struct FileInfoBlock *)AllocDosObject(DOS_FIB, NULL)))
    {
        // We memorize the lock since mapping pointer could possibly
        // be invalidated when reallocated during feedDirEntry()
        // ...pretty nice code design...
        BPTR mappingLock = mapping->lock;

        // Actually read the directory, and allocate the mappings
        if(Examine(mappingLock, fib))
        {
            BPTR oldDir = CurrentDir(mapping->lock);

            while(ExNext(mappingLock, fib))
            {
                if(firstCluster == 0 && v->directory.next + 1 >= v->root_entries)
                {
                    kprintf("VVFAT: too many entries in root directory, using only %ld\n", count);
                    break;
                }

                //kprintf("VVFAT_Create: adding mapping #%ld for object '%s'\n", v->mapping.next, fib->fib_FileName);

                // Create directory entry for this file
                dirEntry = createShortAndLongName(v, i, fib->fib_FileName);
                feedDirEntry(v, mappingIndex, fib, dirEntry);

                count++;
            }
            CurrentDir(oldDir);
        }
        FreeDosObject(DOS_FIB, fib);
    }

    // Fill with zeroes up to the end of the cluster
    while(v->directory.next % (0x10 * v->sectors_per_cluster))
    {
        struct DirEntry *direntry = (struct DirEntry *)arrayGetNext(&v->directory);
        memset(direntry, 0, sizeof(struct DirEntry));
    }

    if(v->fat_type != 32)
    {
        if((mappingIndex == 0) && (v->directory.next < v->root_entries))
        {
            // Root directory
            LONG cur = v->directory.next;

            if(!arrayEnsureAllocated(&v->directory, v->root_entries - 1))
            {
                kprintf("VVFAT: failed to allocated memory for directory.\n");
                return FALSE;
            }
            memset(arrayGet(&v->directory, cur), 0, (v->root_entries - cur) * sizeof(struct DirEntry));
        }
    }

    // Re-get the mapping, since mapping pointer was possibly reallocated during feedDirEntry()
    // ...pretty nice code design...
    mapping = (struct Mapping *)arrayGet(&v->mapping, mappingIndex);
    if(firstCluster == 0)
    {
        firstCluster = 2;
    }
    else
    {
        firstCluster += (v->directory.next - mapping->info.dir.first_dir_index) * 0x20 / v->cluster_size;
    }
    mapping->end = firstCluster;

    dirEntry = (struct DirEntry *)arrayGet(&v->directory, mapping->dir_index);
    setBeginOfDirEntry(dirEntry, mapping->begin);

    return TRUE;
}

/* /// */

/* /// "Initializations" */

static BOOL initFAT(struct VVFAT *v)
{
    UBYTE *fat1stAdr =  (UBYTE*)fatAddress(v, 0);
    struct BootSector *bootSector;

    //kprintf("VVFAT_Create: initFAT()\n");

    if(fat1stAdr && GetHashDataByKey(v->sector_cache_table, BOOT_SECTOR(v), (APTR *)&bootSector))
    {
        *fat1stAdr = bootSector->media_type;

        if(v->fat_type == 12)
        {
            v->max_fat_value = 0xfff;
        }
        else if(v->fat_type == 16)
        {
            v->max_fat_value = 0xffff;
        }
        else /* if(v->fat_type == 32) */
        {
            v->max_fat_value = 0x0fffffff;
        }

        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static BOOL initDirectories(struct VVFAT *v)
{
    BOOL success = FALSE;
    struct BootSector *bootSector;
    struct Mapping *mapping;
    ULONG i;
    ULONG cluster;
    UQUAD volumeSectorCount = 0;
    UQUAD tmpsc;

    //kprintf("VVFAT_Create: initDirectories()\n");

    v->cluster_size   = v->sectors_per_cluster * 0x200;
    v->cluster_buffer = (UBYTE*)AllocPooled(v->sector_cache_mempool, v->cluster_size);

    if(v->cluster_buffer)
    {
        if(!GetHashDataByKey(v->sector_cache_table, BOOT_SECTOR(v), (APTR *)&bootSector))
        {
            goto exit;
        }

        memset(bootSector, 0, sizeof(struct BootSector));

        volumeSectorCount = v->sector_count - v->offset_to_bootsector;
        tmpsc = volumeSectorCount - v->reserved_sectors - v->root_entries / 16;

        v->cluster_count = (tmpsc * 0x200) / ((v->sectors_per_cluster * 0x200) + v->fat_type / 4);

        v->sectors_per_fat  = ((v->cluster_count + 2) * v->fat_type / 8) / 0x200;
        v->sectors_per_fat += (((v->cluster_count + 2) * v->fat_type / 8) % 0x200) > 0;

        v->offset_to_fat = v->offset_to_bootsector + v->reserved_sectors;
        v->offset_to_root_dir = v->offset_to_fat + v->sectors_per_fat * NUMBER_OF_FAT;
        v->offset_to_data = v->offset_to_root_dir + v->root_entries / 16;

        arrayInit(&v->mapping, sizeof(struct Mapping));
        arrayInit(&v->directory, sizeof(struct DirEntry));

        // Set first entry to volume label
        feedVolumeLabel(v, (struct DirEntry *)arrayGetNext(&v->directory));

        // Now build FAT, and write back information into directory
        if(!initFAT(v))
        {
            goto exit;
        }

        // Create initial cluster mapping
        mapping = (struct Mapping *)arrayGetNext(&v->mapping);
        mapping->begin = 0;
        mapping->dir_index = 0;
        mapping->info.dir.parent_mapping_index = -1;
        mapping->first_mapping_index = -1;
        mapping->lock = DupLock(v->dir_lock);
        mapping->mode = MODE_DIRECTORY;
        mapping->read_only = 0;

        for(i=0, cluster=v->first_cluster_of_root_dir; i<v->mapping.next; i++)
        {
            // Fix FAT entry if not root directory of FAT12/FAT16
            BOOL fixFAT = (cluster != 0);

            mapping = (struct Mapping *)arrayGet(&v->mapping, i);

            if(mapping->mode & MODE_DIRECTORY)
            {
                mapping->begin = cluster;

                if(!readDirectory(v, i))
                {
                    STRPTR buffer = getNameFromLock(mapping->lock);
                    kprintf("VVFAT: could not read directory '%s'\n", buffer ? buffer : "???");
                    FreeVec(buffer);
                    goto exit;
                }
                mapping = (struct Mapping *)arrayGet(&v->mapping, i);
            }
            else
            {
                assert(mapping->mode == MODE_UNDEFINED);

                mapping->mode = MODE_FILE;
                mapping->begin = cluster;

                if(mapping->end > 0)
                {
                    struct DirEntry *direntry = (struct DirEntry *)arrayGet(&v->directory, mapping->dir_index);

                    mapping->end = cluster + 1 + (mapping->end-1) / v->cluster_size;
                    setBeginOfDirEntry(direntry, mapping->begin);
                }
                else
                {
                    mapping->end = cluster + 1;
                    fixFAT = FALSE;
                }
            }

            assert(mapping->begin < mapping->end);

            /* Next free cluster */
            cluster = mapping->end;

            if(cluster >= (v->cluster_count + 2))
            {
                kprintf("VVFAT: directory does not fit in FAT%d (capacity %ld MB).\n", v->fat_type, v->sector_count >> 11);
                goto exit;
            }

            // Fix FAT for entry
            if(fixFAT)
            {
                ULONG j;

                for(j=mapping->begin; j<mapping->end-1; j++)
                {
                    if(!fatSet(v, j, j + 1))
                    {
                        kprintf("VVFAT: failed to set data in FAT.\n");
                        goto exit;
                    }
                }
                if(!fatSet(v, mapping->end - 1, v->max_fat_value))
                {
                    kprintf("VVFAT: failed to set data in FAT.\n");
                    goto exit;
                }
            }

            //{ STRPTR buffer = getNameFromLock(mapping->lock); kprintf("Created mapping of %s (%p): [%p]%s -> [%ld,%ld]\n", buffer ? buffer : "???", mapping, mapping->lock, mapping->read_only ? "*" : "", mapping->begin, mapping->end); FreeVec(buffer); }
        }

        mapping = (struct Mapping *)arrayGet(&v->mapping, 0);
        assert((v->fat_type == 32) || (mapping->end == 2));

        // The FAT signature
        if(!fatSet(v, 0, v->max_fat_value)
        || !fatSet(v, 1, v->max_fat_value))
        {
            kprintf("VVFAT: failed to set data in FAT.\n");
            goto exit;
        }

        if(!fatCreateSpare(v))
        {
            kprintf("VVFAT: failed to create spare FAT.\n");
            goto exit;

        }

        v->current_mapping = NULL;

        bootSector->jump[0] = 0xeb;

        if(v->fat_type != 32)
        {
            bootSector->jump[1] = 0x3e;
        }
        else
        {
            bootSector->jump[1] = 0x58;
        }

        bootSector->jump[2] = 0x90;

        memcpy(bootSector->name, BOOTSECTOR_OEM_NAME, 8);

        bootSector->sector_size = BE_SWAPWORD(0x200);
        bootSector->sectors_per_cluster = v->sectors_per_cluster;
        bootSector->reserved_sectors = BE_SWAPWORD(v->reserved_sectors);
        bootSector->number_of_fats = NUMBER_OF_FAT;

        if(v->fat_type != 32)
        {
            bootSector->root_entries = BE_SWAPWORD(v->root_entries);
        }
        bootSector->total_sectors16 = (volumeSectorCount > 0xffff) ? 0 : BE_SWAPWORD(volumeSectorCount);
        bootSector->media_type = ((v->fat_type != 12) ? 0xf8:0xf0);

        if(v->fat_type != 32)
        {
            bootSector->sectors_per_fat = BE_SWAPWORD(v->sectors_per_fat);
        }

        bootSector->sectors_per_track = BE_SWAPWORD(v->spt);
        bootSector->number_of_heads = BE_SWAPWORD(v->heads);
        bootSector->hidden_sectors = BE_SWAPLONG(v->offset_to_bootsector);
        bootSector->total_sectors = BE_SWAPLONG((volumeSectorCount > 0xffff) ? volumeSectorCount : 0);

        if(v->fat_type != 32)
        {
            bootSector->u.fat16.drive_number = (v->fat_type == 12) ? 0:0x80; // assume this is hda (TODO)
            bootSector->u.fat16.signature = 0x29;
            bootSector->u.fat16.id = BE_SWAPLONG(0xfabe1afd);
            memcpy(bootSector->u.fat16.volume_label, FAT_VOLUME_LABEL, 11);
            memcpy(bootSector->u.fat16.fat_type, (v->fat_type==12) ? "FAT12   ":"FAT16   ", 8);
        }
        else
        {
            bootSector->u.fat32.sectors_per_fat = BE_SWAPLONG(v->sectors_per_fat);
            bootSector->u.fat32.first_cluster_of_root_dir = BE_SWAPLONG(v->first_cluster_of_root_dir);
            bootSector->u.fat32.info_sector = BE_SWAPWORD(1);
            bootSector->u.fat32.backup_boot_sector = BE_SWAPWORD(6);
            bootSector->u.fat32.drive_number = 0x80; // assume this is hda (TODO)
            bootSector->u.fat32.signature = 0x29;
            bootSector->u.fat32.id = BE_SWAPLONG(0xfabe1afd);
            memcpy(bootSector->u.fat32.volume_label, FAT_VOLUME_LABEL, 11);
            memcpy(bootSector->u.fat32.fat_type, "FAT32   ", 8);
        }

        bootSector->magic[0] = 0x55;
        bootSector->magic[1] = 0xaa;

        if(v->fat_type == 32)
        {
            struct BootSector *bootSectorBackup;
            struct InfoSector *infoSector;

            if(!GetHashDataByKey(v->sector_cache_table, BOOT_SECTOR_BACKUP(v), (APTR *)&bootSectorBackup)
            || !GetHashDataByKey(v->sector_cache_table, INFO_SECTOR(v), (APTR *)&infoSector))
            {
                goto exit;
            }

            // Backup boot sector
            memcpy(bootSectorBackup, bootSector, 0x200);
            // FS info sector
            memset(infoSector, 0, sizeof(struct InfoSector));
            infoSector->signature1 = BE_SWAPLONG(0x41615252);
            infoSector->signature2 = BE_SWAPLONG(0x61417272);
            infoSector->free_clusters = BE_SWAPLONG(v->cluster_count - cluster + 2);
            infoSector->mra_cluster = BE_SWAPLONG(2);
            infoSector->magic[0] = 0x55;
            infoSector->magic[1] = 0xaa;
        }
        success = TRUE;
    }

exit:
    return success;
}

static BOOL initMBR(struct VVFAT *v)
{
    struct MBR *mbr;
    struct Partition *partition;
    BOOL lba;

    //kprintf("VVFAT_Create: initMBR()\n");

    if(!GetHashDataByKey(v->sector_cache_table, MBR_SECTOR(v), (APTR *)&mbr))
    {
        return FALSE;
    }

    memset(mbr, 0, sizeof(struct MBR));

    partition = &mbr->partition[0];

    // Win NT Disk Signature
    mbr->nt_id = BE_SWAPLONG(0xbe1afdfa);

    partition->attributes = PARTITION_ATTRIBUTE_BOOTABLE;

    // LBA is used when partition is outside the CHS geometry
    lba = sector2CHS(v, v->offset_to_bootsector, &partition->start_CHS)
        | sector2CHS(v, v->sector_count - 1, &partition->end_CHS);

    //kprintf("VVFAT_Create: initMBR() - LBA: %s\n", lba ? "Yes" : "No");

    // LBA partitions are identified only by start/length_sector_long not by CHS
    partition->start_sector_long = BE_SWAPLONG(v->offset_to_bootsector);
    partition->length_sector_long = BE_SWAPLONG(v->sector_count - v->offset_to_bootsector);

    /* FAT12/FAT16/FAT32 */
    /* DOS uses different types when partition is LBA,
       probably to prevent older versions from using CHS on them */
    if(v->fat_type == 12)
    {
        partition->fs_type = PARTITION_TYPE_FAT12;
    }
    else if(v->fat_type == 16)
    {
        partition->fs_type = lba ? PARTITION_TYPE_FAT16L : PARTITION_TYPE_FAT16;
    }
    else /* if(v->faT_Type == 32) */
    {
        partition->fs_type = lba ? PARTITION_TYPE_FAT32L : PARTITION_TYPE_FAT32;
    }

    mbr->magic[0] = 0x55;
    mbr->magic[1] = 0xaa;

    return TRUE;
}

static BOOL init(struct VVFAT *v, struct VVFAT_Configuration *cfg)
{
    BOOL success = FALSE;
    ULONG sizeInMB;
    UBYTE *specialSectors;

    //kprintf("VVFAT_Create: init()\n");

    v->sector_cache_table = CreateHashTableTagList(NULL);
    v->sector_cache_mempool = CreatePool(MEMF_PUBLIC, 0x200, 0x200);

    if(v->sector_cache_table == NULL || v->sector_cache_mempool == NULL)
    {
        return FALSE;
    }

    // Automatic geometry?
    if(cfg == NULL)
    {
        struct InfoData info;
        ULONG sectorCount;

        if(!Info(v->dir_lock, &info))
        {
            kprintf("VVFAT: impossible to create automatic geometry.\n");
            return FALSE;
        }

        sectorCount = info.id_BytesPerBlock / 512 * info.id_NumBlocks;

        if(sectorCount < 32)
        {
            kprintf("VVFAT: unsupported geometry.\n");
            return FALSE;
        }

        v->fat_type = TYPE_FAT32;

        v->spt = 63;
        v->heads = 255;

        while(v->spt * v->heads > sectorCount)
        {
            switch(v->spt)
            {
            case 63: v->spt = 32; break;
            case 32: v->spt = 16; break;
            default:
                v->heads--;
                break;
            }
        }

        v->cylinders = sectorCount / v->spt / v->heads < 65536 ? sectorCount / v->spt / v->heads : 65535;
        //kprintf("Computed geometry: %ld -> spt=%ld, heads=%ld, cylinders=%ld -> %ld\n", sectorCount, v->spt, v->heads, v->cylinders, v->spt * v->heads * v->cylinders);
    }
    else
    {
        if(cfg->type_of_fat != TYPE_FAT12 && cfg->type_of_fat != TYPE_FAT16 && cfg->type_of_fat != TYPE_FAT32)
        {
            kprintf("VVFAT: unsupported VFAT type.\n");
            return FALSE;
        }

        // Set VVFAT type to simulate
        v->fat_type = cfg->type_of_fat;
        // Set faked geometry to use
        v->cylinders = cfg->cylinders;
        v->heads = cfg->heads;
        v->spt = cfg->sectors_per_cylinder;
    }

    v->offset_to_bootsector = v->spt;
    v->sector_count = v->cylinders * v->heads * v->spt;

    sizeInMB = (512LL * v->sector_count) >> 20;

    //kprintf("VVFAT_Create: init() - HD size: %ld MB\n", sizeInMB);

    if(v->fat_type == 32)
    {
        if(sizeInMB >= 32767)
            v->sectors_per_cluster = 64;
        else if(sizeInMB >= 16383)
            v->sectors_per_cluster = 32;
        else if(sizeInMB >= 8191)
            v->sectors_per_cluster = 16;
        else if(sizeInMB >= 4095)
            v->sectors_per_cluster = 8;
        else if(sizeInMB >= 2047)
            v->sectors_per_cluster = 4;
        else
            v->sectors_per_cluster = 2;

        v->first_cluster_of_root_dir = 2;
        v->root_entries = 0;
        v->reserved_sectors = 32;
    }
    else
    {
        if(sizeInMB >= 1023)
            v->sectors_per_cluster = 64;
        else if(sizeInMB >= 511)
            v->sectors_per_cluster = 32;
        else if(sizeInMB >= 255)
            v->sectors_per_cluster = 16;
        else if(sizeInMB >= 127)
            v->sectors_per_cluster = 8;
        else if(sizeInMB >= 63)
            v->sectors_per_cluster = 4;
        else
            v->sectors_per_cluster = 2;

        v->first_cluster_of_root_dir = 0;
        v->root_entries = 512;
        v->reserved_sectors = 1;
    }

    v->current_cluster = (ULONG)-1;
    v->current_fh = (BPTR)0;

    // Allocate MBR and BOOT special sectors, plus BOOT backup and INFO sector for FAT32
    specialSectors = (UBYTE*)AllocPooled(v->sector_cache_mempool, v->fat_type == 32 ? 0x800 : 0x400);

    if(specialSectors)
    {
        // Add special sectors to sector cache hash table for generic access
        InsertHash(v->sector_cache_table, MBR_SECTOR(v), &specialSectors[0x000]);
        InsertHash(v->sector_cache_table, BOOT_SECTOR(v), &specialSectors[0x200]);

        if(v->fat_type == 32)
        {
            InsertHash(v->sector_cache_table, BOOT_SECTOR_BACKUP(v), &specialSectors[0x400]);
            InsertHash(v->sector_cache_table, INFO_SECTOR(v), &specialSectors[0x600]);
        }

        if(initMBR(v)
        && initDirectories(v))
        {
            //{ STRPTR buffer = getNameFromLock(v->dir_lock); kprintf("'vvfat' disk opened: directory is '%s'.\n", buffer ? buffer : "???"); FreeVec(buffer); }

            success = TRUE;
        }
    }

    return success;
}

static VOID clear(struct VVFAT *v)
{
    ULONG i;

    if(v->modified)
    {
        synchronize(v);
    }

    arrayFree(&v->directory);
    for(i=0; i<v->mapping.next; i++)
    {
        struct Mapping *mapping = (struct Mapping *)arrayGet(&v->mapping, i);
        UnLock(mapping->lock);
    }
    arrayFree(&v->mapping);

    DeleteHashTable(v->sector_cache_table);
    if(v->sector_cache_mempool)
    {
        DeletePool(v->sector_cache_mempool);
    }
    Close(v->current_fh);
}

/* /// */

/* /// "Public functions" */

struct VVFAT * VVFAT_Create(CONST_STRPTR dirName, struct VVFAT_Configuration *cfg)
{

    struct VVFAT *v = (struct VVFAT*)AllocVec(sizeof(struct VVFAT), MEMF_PUBLIC | MEMF_CLEAR);

    //kprintf("VVFAT_Create(%s)\n", dirName);

    if(v)
    {
        v->dir_lock = Lock(dirName, SHARED_LOCK);

        if(v->dir_lock)
        {
            // Init VVFAT
            if(init(v, cfg))
            {
                return v;
            }
        }
    }

    VVFAT_Destroy(v);

    return NULL;
}

VOID VVFAT_Destroy(struct VVFAT *v)
{
    //kprintf("VVFAT_Destroy()\n");

    if(v)
    {
        clear(v);
        UnLock(v->dir_lock);
        FreeVec(v);
    }
}

BOOL VVFAT_ReadData(struct VVFAT *v, APTR buffer, UQUAD offset, ULONG size)
{
    if(v)
    {
        UBYTE *cbuf = (UBYTE *)buffer;
        ULONG scount = size / 0x200;
        ULONG sectorNum = offset / 0x200;

        while(scount-- > 0)
        {
            APTR cachedSector;

            // Sector in cache?
            if(GetHashDataByKey(v->sector_cache_table, sectorNum, &cachedSector))
            {
                memcpy(cbuf, cachedSector, 0x200);
                //kprintf("VVFAT read from cache (%ld)\n", sectorNum);
            }
            // Sector bellow data?
            else if(sectorNum < v->offset_to_data)
            {
                memset(cbuf, 0, 0x200);
                //kprintf("VVFAT read empty sector (%ld)\n", sectorNum);
            }
            else
            {
                ULONG sector = sectorNum - v->offset_to_data;
                ULONG sectorOffsetInCluster = sector % v->sectors_per_cluster;
                ULONG clusterNum = sector / v->sectors_per_cluster + 2;

                if(readCluster(v, clusterNum))
                {
                    memcpy(cbuf, v->cluster + sectorOffsetInCluster * 0x200, 0x200);
                    //kprintf("VVFAT read from mapping (%ld)\n", sectorNum);
                }
                else
                {
                    memset(cbuf, 0, 0x200);
                    //kprintf("VVFAT read unmapped sector (%ld)\n", sectorNum);
                }
            }
            sectorNum++;
            cbuf += 0x200;
        }
        return TRUE;
    }
    return FALSE;
}

BOOL VVFAT_WriteData(struct VVFAT *v, const APTR buffer, UQUAD offset, ULONG size)
{
    if(v)
    {
        UBYTE *cbuf = (UBYTE *)buffer;
        ULONG scount = size / 0x200;
        ULONG sectorNum = offset / 0x200;

        while(scount-- > 0)
        {
            APTR cachedSector;

            // Already in cache?
            if(!GetHashDataByKey(v->sector_cache_table, sectorNum, &cachedSector))
            {
                cachedSector = AllocPooled(v->sector_cache_mempool, 0x200);

                if(cachedSector)
                {
                    InsertHash(v->sector_cache_table, sectorNum, cachedSector);
                }
            }

            if(cachedSector)
            {
                memcpy(cachedSector, cbuf, 0x200);
            }
            //kprintf("VVFAT write to cache (%ld)\n", sectorNum);

            v->modified = TRUE;

            sectorNum++;
            cbuf += 0x200;
        }
        return TRUE;
    }
    return FALSE;
}

/* /// */

