/****** x-mass.acepansion/background ****************************************
*
* DESCRIPTION
*  This plugin provides emulation of Tot0's X-Mass expansion card which is
*  compatible with Symbiface II IDE interface.
*
* HISTORY
*
*****************************************************************************
*
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#ifndef __HAIKU__
#include <proto/muimaster.h>
#include <proto/intuition.h>

#include <libraries/asl.h>
#include <libraries/mui.h>
#endif

#define USE_INLINE_API
#include <acepansion/lib_header.h>
#include <libraries/acepansion_plugin.h>

#include "ide.h"
#include "acepansion.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

#ifdef __HAIKU__
#include <Box.h>
#include <Button.h>
#include <CheckBox.h>
#include <GroupView.h>
#include <LayoutBuilder.h>
#include <OptionPopUp.h>
#include <StringView.h>

#include "sys/haiku/PopASL.h"
#endif


struct Library *DOSBase;
struct Library *UtilityBase;
struct Library *IntuitionBase;
struct Library *MUIMasterBase;
struct Library *HashTableBase;



struct PluginData
{
    // Plugin generic part (do NEVER put anything before this!)
    struct ACEpansionPlugin common;

    // X-Mass specifics
    struct IDE *ide;
    BOOL resetTrigger;
    // MUI prefs
    struct SignalSemaphore *sema;
#ifdef __HAIKU__
    BOptionPopUp *CYA_Mode;
    BCheckBox *BTN_ImageFileMount;
    PopASL *STR_ImageFileName;
    PopASL *STR_DirectoryName;
#else
    struct Hook mountHook;
    Object *CYA_Mode;
    Object *BTN_ImageFileMount;
    Object *STR_ImageFileName;
    Object *STR_DirectoryName;
#endif
    Object *BTN_ImageFileName;
    Object *BTN_DirectoryName;
};



struct Msg_MountHandler
{
    IPTR dummy;
};

static VOID mount(struct PluginData *myPlugin);
static LONG mountHandler(VOID);
#ifndef __HAIKU__
static struct EmulLibEntry mountGate = { TRAP_LIB, 0, (VOID (*)(VOID))&mountHandler };
#endif

static STRPTR ModeCycleEntries[] =
{
    (STRPTR)MSG_MODE_DIRECTORY,
    (STRPTR)MSG_MODE_IMAGEFILE,
    NULL
};

#define TOOLTYPE_DIRECTORY "DIRECTORY"
#define TOOLTYPE_IMAGEFILE "IMAGEFILE"
#define TOOLTYPE_AUTOMOUNT "AUTOMOUNT"
#define TOOLTYPE_MOUNTMODE "MOUNTMODE"

#define TOOLTYPE_MOUNTMODE_DIRECTORY "Directory"
#define TOOLTYPE_MOUNTMODE_IMAGEFILE "ImageFile"

#define MODE_DIRECTORY 0
#define MODE_IMAGEFILE 1


#ifdef __HAIKU__
class SettingsWindow: public BGroupView
{
    public:
        SettingsWindow(struct PluginData* plugin, BView*, BView*, int mode);

        void AttachedToWindow() override;
        void MessageReceived(BMessage*) override;

    private:
        void MountHook();

        struct PluginData* myPlugin;
        BCardLayout *GRP_Mode;
        BButton *BTN_Synchronize;
};

SettingsWindow::SettingsWindow(struct PluginData* plugin, BView* dirBox, BView* fileBox, int mode)
    : BGroupView(B_VERTICAL)
    , myPlugin(plugin)
{
    BBox* box = new BBox("mode");
    box->SetLabel(GetString(MSG_MODE_GROUP));
    BGroupView* inner = new BGroupView(B_HORIZONTAL);
    box->AddChild(inner);

    BLayoutBuilder::Group<>(inner)
        .SetInsets(B_USE_SMALL_SPACING)
        .Add(myPlugin->CYA_Mode = new BOptionPopUp("", "", new BMessage('mode')))
        .Add(BTN_Synchronize = new BButton(GetString(MSG_SYNCHRONIZE), nullptr))
    .End();

    for (int i = 0; ModeCycleEntries[i]; i++) {
        ModeCycleEntries[i] = GetString((intptr_t)ModeCycleEntries[i]);
        myPlugin->CYA_Mode->AddOption(ModeCycleEntries[i], i);
        myPlugin->CYA_Mode->SetToolTip(GetString(MSG_MODE_HELP));
    }

    BLayoutBuilder::Group<>(this)
        .Add(box)
        .AddCards()
            .GetLayout(&GRP_Mode)
            .Add(dirBox)
            .Add(fileBox)
            .SetVisibleItem(mode)
        .End()
    .End();
}

void SettingsWindow::AttachedToWindow()
{
    myPlugin->CYA_Mode->SetTarget(this);
    myPlugin->STR_DirectoryName->SetTarget(this);
    myPlugin->STR_ImageFileName->SetTarget(this);
    myPlugin->STR_DirectoryName->TextControl()->SetMessage(new BMessage('sdir'));
    myPlugin->STR_ImageFileName->TextControl()->SetMessage(new BMessage('file'));
    myPlugin->BTN_ImageFileMount->SetTarget(this);
    BTN_Synchronize->SetTarget(this);

    BGroupView::AttachedToWindow();
}

void SettingsWindow::MessageReceived(BMessage* message)
{
    switch(message->what) {
        case 'mode':
        {
            // Mode select
            GRP_Mode->SetVisibleItem(message->FindInt32("be:value"));
            MountHook();
            break;
        }
        case 'file':
        {
            // Selecting a new image go back to unmount state
            myPlugin->BTN_ImageFileMount->SetValue(B_CONTROL_OFF);
            MountHook();
            break;
        }
        case 'sdir':
        case 'mont':
        case 'sync':
        {
            // Change directory will actually mount
            // Click mount/create will actually mount/create
            // Manual synchronization
            MountHook();
            break;
        }
        default:
        {
            BGroupView::MessageReceived(message);
            break;
        }
    }
}
#endif


#ifndef __HAIKU__
/*
** Private function which is called to initialize commons
** just after the library was loaded into memory and prior
** to any other API call.
**
** This is the good place to open our required libraries.
*/
BOOL InitResources(VOID)
{
    DOSBase = OpenLibrary("dos.library", 0L);
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);
    HashTableBase = OpenLibrary("hashtable.library", 0L);

    ModeCycleEntries[0] = GetString((ULONG)ModeCycleEntries[0]);
    ModeCycleEntries[1] = GetString((ULONG)ModeCycleEntries[1]);

    return DOSBase && UtilityBase && IntuitionBase && MUIMasterBase && HashTableBase;
}



/*
** Private function which is called to free commons
** just before the library is expurged from memory
**
** This is the good place to close our required libraries.
*/
VOID FreeResources(VOID)
{
    CloseLibrary(DOSBase);
    CloseLibrary(UtilityBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(MUIMasterBase);
    CloseLibrary(HashTableBase);
}
#endif



/****** x-mass.acepansion/CreatePlugin **************************************
*
* NAME
*   CreatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
*
* FUNCTION
*   Create the plugin data structure and configure how it will be handled by
*   ACE. This function is usually called at ACE startup.
*
* INPUTS
*   *toolType
*     A pointer to a NULL terminated array of CONST_STRPTR containing the
*     tooltypes provided with the plugin library.
*   *sema
*     Pointer to a semaphore to use to protect access to custom data from
*     ACEpansionPlugin when accessed from subtasks or within your PrefsWindow.
*
* RESULT
*   A pointer to an allocated and initialized plugin data structure.
*
* SEE ALSO
*   DeletePlugin
*   ActivatePlugin
*   GetPrefsPlugin
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, UNUSED struct SignalSemaphore *sema)
{
    // We are allocating an extended plugin structure to add our custom stuff
    struct PluginData *myPlugin = (struct PluginData *)AllocVec(sizeof(struct PluginData), MEMF_CLEAR);

    if(myPlugin)
    {
#ifndef __HAIKU__
        Object *GRP_Mode;
        Object *BTN_Synchronize;
#endif
        CONST_STRPTR imageFileName = "Share/X-Mass.img";
        CONST_STRPTR directoryName = "Share/";
        BOOL automount = FALSE;
        LONG mode = MODE_DIRECTORY;

        // Initialize plugin specifics
        myPlugin->ide = IDECreate();
        myPlugin->sema = sema;
#ifndef __HAIKU__
        myPlugin->mountHook.h_MinNode.mln_Succ = NULL;
        myPlugin->mountHook.h_MinNode.mln_Pred = NULL;
        myPlugin->mountHook.h_Entry = (HOOKFUNC)&mountGate;
        myPlugin->mountHook.h_SubEntry = NULL;
        myPlugin->mountHook.h_Data = (APTR)myPlugin;
#endif

        if(myPlugin->ide)
        {
            // Get tooltypes to configure ourself
            if(toolTypes) while(*toolTypes)
            {
                CONST_STRPTR ptr = *toolTypes;
                CONST_STRPTR name = ptr;
                ULONG len = 0;

                if(*ptr != '(')
                {
                    while(*ptr != '=' && *ptr != '\0')
                    {
                        ptr++;
                        len++;
                    }

                    if(*ptr == '=')
                    {
                        if(Strnicmp(name,TOOLTYPE_DIRECTORY,len) == 0)
                        {
                            directoryName = ptr+1;
                        }
                        else if(Strnicmp(name,TOOLTYPE_IMAGEFILE,len) == 0)
                        {
                            imageFileName = ptr+1;
                        }
                        else if(Strnicmp(name,TOOLTYPE_MOUNTMODE,len) == 0)
                        {
                            CONST_STRPTR value = ptr+1;

                            if(Stricmp(value,TOOLTYPE_MOUNTMODE_DIRECTORY) == 0)
                            {
                                mode = MODE_DIRECTORY;
                            }
                            else if(Stricmp(value,TOOLTYPE_MOUNTMODE_IMAGEFILE) == 0)
                            {
                                mode = MODE_IMAGEFILE;
                            }
                        }
                    }
                    else if(Strnicmp(name,TOOLTYPE_AUTOMOUNT,len) == 0)
                    {
                        automount = TRUE;
                    }
                }
                toolTypes++;
            }

            // Initialize plugin generic part
            myPlugin->common.ap_APIVersion     = API_VERSION;
            myPlugin->common.ap_Flags          = ACE_FLAGSF_ACTIVE_WRITEIO
                                               | ACE_FLAGSF_ACTIVE_READIO
                                               | ACE_FLAGSF_ACTIVE_RESET
                                               | ACE_FLAGSF_ACTIVE_EMULATE
                                               | ACE_FLAGSF_SAVE_PREFS;
            myPlugin->common.ap_Title          = GetString(MSG_TITLE);
            myPlugin->common.ap_HelpFileName   = LIBNAME".guide";
            myPlugin->common.ap_ToggleMenuName = GetString(MSG_MENU_TOGGLE);
            myPlugin->common.ap_PrefsMenuName  = GetString(MSG_MENU_PREFS);
#ifdef __HAIKU__

            BBox* dirBox = new BBox("directory");
            dirBox->SetLabel(GetString(MSG_DIRECTORY_GROUP));
            BGroupView* inner = new BGroupView(B_HORIZONTAL);
            dirBox->AddChild(inner);

            BLayoutBuilder::Group<>(inner)
                .SetInsets(B_USE_SMALL_SPACING)
                .Add(new BStringView("", GetString(MSG_DIRECTORY)))
                .Add(myPlugin->STR_DirectoryName = new PopASL(0, nullptr, directoryName,
                    GetString(MSG_DIRECTORY_ASL_REQUEST), nullptr, GetString(MSG_DIRECTORY_HELP),
                    B_OPEN_PANEL, true, B_DIRECTORY_NODE))
            .End();

            BBox* fileBox = new BBox("file");
            fileBox->SetLabel(GetString(MSG_IMAGE_FILE_GROUP));
            inner = new BGroupView(B_VERTICAL);
            fileBox->AddChild(inner);

            BLayoutBuilder::Group<>(inner)
                .SetInsets(B_USE_SMALL_SPACING)
                .AddGroup(B_HORIZONTAL)
                    .Add(new BStringView(nullptr, GetString(MSG_IMAGE_FILE)))
                    .Add(myPlugin->STR_ImageFileName = new PopASL(1, nullptr, imageFileName, nullptr,
                        GetString(MSG_IMAGE_ASL_REQUEST), GetString(MSG_IMAGE_FILE_HELP)))
                    .Add(myPlugin->BTN_ImageFileMount = new BCheckBox(nullptr,
                        GetString(MSG_IMAGE_MOUNT), new BMessage('mont')))
                .End()
                .Add(new BStringView(nullptr, GetString(MSG_IMAGE_NOTICE)))
            .End();

            if (automount)
                myPlugin->BTN_ImageFileMount->SetValue(B_CONTROL_ON);

            SettingsWindow* gv = new SettingsWindow(myPlugin, dirBox, fileBox, mode);
            myPlugin->common.ap_PrefsWindow = (Object*)gv;
#else
            myPlugin->common.ap_PrefsWindow    = VGroup,
                Child, HGroup,
                    GroupFrameT(GetString(MSG_MODE_GROUP)),
                    Child, myPlugin->CYA_Mode = CycleObject,
                        MUIA_Cycle_Entries, &ModeCycleEntries,
                        MUIA_CycleChain, TRUE,
                        MUIA_ShortHelp, GetString(MSG_MODE_HELP),
                        End,
                    Child, BTN_Synchronize = TextObject,
                        ButtonFrame,
                        MUIA_Weight, 0,
                        MUIA_InputMode , MUIV_InputMode_RelVerify,
                        MUIA_Background, MUII_ButtonBack,
                        MUIA_CycleChain, TRUE,
                        MUIA_Text_Contents, GetString(MSG_SYNCHRONIZE),
                        MUIA_ShortHelp, GetString(MSG_SYNCHRONIZE_HELP),
                        End,
                    End,
                Child, GRP_Mode = VGroup,
                    MUIA_Group_PageMode, TRUE,
                    MUIA_Group_ActivePage, mode,
                    Child, VGroup,
                        GroupFrameT(GetString(MSG_DIRECTORY_GROUP)),
                        Child, VGroup,
                            MUIA_Group_Columns, 2,
                            Child, Label2(GetString(MSG_DIRECTORY)),
                            Child, PopaslObject,
                                MUIA_Popstring_String, myPlugin->STR_DirectoryName = StringObject,
                                    StringFrame,
                                    MUIA_String_SpellChecking, FALSE,
                                    MUIA_String_Contents, directoryName,
                                    MUIA_CycleChain, TRUE,
                                    End,
                                MUIA_Popstring_Button, myPlugin->BTN_DirectoryName = TextObject,
                                    ButtonFrame,
                                    MUIA_Weight, 0,
                                    MUIA_InputMode , MUIV_InputMode_RelVerify,
                                    MUIA_Background, MUII_ButtonBack,
                                    MUIA_CycleChain, TRUE,
                                    MUIA_Text_Contents, "\33I[6:19]", // MUII_PopFile
                                    End,
                                ASLFR_TitleText, GetString(MSG_DIRECTORY_ASL_REQUEST),
                                ASLFR_DrawersOnly, TRUE,
                                MUIA_ShortHelp, GetString(MSG_DIRECTORY_HELP),
                                End,
                            End,
                        End,
                    Child, VGroup,
                        GroupFrameT(GetString(MSG_IMAGE_FILE_GROUP)),
                        Child, VGroup,
                            MUIA_Group_Columns, 3,
                            Child, Label2(GetString(MSG_IMAGE_FILE)),
                            Child, PopaslObject,
                                MUIA_Popstring_String, myPlugin->STR_ImageFileName = StringObject,
                                    StringFrame,
                                    MUIA_String_SpellChecking, FALSE,
                                    MUIA_String_Contents, imageFileName,
                                    MUIA_CycleChain, TRUE,
                                    End,
                                MUIA_Popstring_Button, myPlugin->BTN_ImageFileName = TextObject,
                                    ButtonFrame,
                                    MUIA_Weight, 0,
                                    MUIA_InputMode , MUIV_InputMode_RelVerify,
                                    MUIA_Background, MUII_ButtonBack,
                                    MUIA_CycleChain, TRUE,
                                    MUIA_Text_Contents, "\33I[6:19]", // MUII_PopFile
                                    End,
                                ASLFR_TitleText, GetString(MSG_IMAGE_ASL_REQUEST),
                                MUIA_ShortHelp, GetString(MSG_IMAGE_FILE_HELP),
                                End,
                            Child, myPlugin->BTN_ImageFileMount = TextObject,
                                ButtonFrame,
                                MUIA_Weight, 0,
                                MUIA_InputMode , MUIV_InputMode_Toggle,
                                MUIA_Background, MUII_ButtonBack,
                                MUIA_Selected, automount,
                                MUIA_CycleChain, TRUE,
                                MUIA_Text_PreParse, MUIX_C,
                                MUIA_Text_Contents, GetString(MSG_IMAGE_MOUNT),
                                MUIA_ShortHelp, GetString(MSG_IMAGE_MOUNT_HELP),
                                End,
                            End,
                        Child, TextObject,
                            MUIA_Font, MUIV_Font_Tiny,
                            MUIA_Text_Contents, GetString(MSG_IMAGE_NOTICE),
                            MUIA_Text_SetMax, FALSE,
                            MUIA_Text_SetMin, FALSE,
                            End,
                        End,
                    End,
                End;
#endif

#ifndef __HAIKU__
            // Mode select
            DoMethod(myPlugin->CYA_Mode,MUIM_Notify,MUIA_Cycle_Active,MUIV_EveryTime,
                     GRP_Mode,3,MUIM_Set,MUIA_Group_ActivePage,MUIV_TriggerValue);
            DoMethod(myPlugin->CYA_Mode,MUIM_Notify,MUIA_Cycle_Active,MUIV_EveryTime,
                     MUIV_Notify_Self,2,MUIM_CallHook,&myPlugin->mountHook);
            // Change directory will actually mount
            DoMethod(myPlugin->STR_DirectoryName,MUIM_Notify,MUIA_String_Acknowledge,MUIV_EveryTime,
                     MUIV_Notify_Self,2,MUIM_CallHook,&myPlugin->mountHook);
            // Click mount/create will actually mount/create
            DoMethod(myPlugin->BTN_ImageFileMount,MUIM_Notify,MUIA_Selected,MUIV_EveryTime,
                     MUIV_Notify_Self,2,MUIM_CallHook,&myPlugin->mountHook);
            // Selecting a new image go back to unmount state
            DoMethod(myPlugin->STR_ImageFileName,MUIM_Notify,MUIA_String_Acknowledge,MUIV_EveryTime,
                     myPlugin->BTN_ImageFileMount,3,MUIM_Set,MUIA_Selected,FALSE);
            // Manual synchronization
            DoMethod(BTN_Synchronize,MUIM_Notify,MUIA_Pressed,FALSE,
                     MUIV_Notify_Self,2,MUIM_CallHook,&myPlugin->mountHook);
#endif
        }
        else
        {
            FreeVec(myPlugin);
            myPlugin = NULL;
        }
    }

    return (struct ACEpansionPlugin *)myPlugin;
}



/****** x-mass.acepansion/DeletePlugin **************************************
*
* NAME
*   DeletePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   VOID DeletePlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Delete a plugin data structure. This function is usually called when ACE
*   is exiting.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to delete.
*
* RESULT
*
* SEE ALSO
*   CreatePlugin
*   ActivatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID DeletePlugin(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

#ifdef __HAIKU__
    delete myPlugin->common.ap_PrefsWindow;
#else
    MUI_DisposeObject(myPlugin->common.ap_PrefsWindow);
#endif
    IDEDestroy(myPlugin->ide);
    FreeVec(myPlugin);
}



/****** x-mass.acepansion/ActivatePlugin ************************************
*
* NAME
*   ActivatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL status)
*
* FUNCTION
*   This function is called everytime ACE wants to activate or disactivate your plugin.
*   While you are disactivated, no other function (except Delete) will the called by ACE.
*   Please note that a plugin is always disactivated by ACE prior to DeletePlugin().
*
* INPUTS
*   activate
*     New activation status requested by ACE.
*
* RESULT
*   Actual activation status (you may fail at activation for some reason, but you
*   should never fail at disactivation request).
*
* SEE ALSO
*   CreatePlugin
*   DeletePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL activate)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(activate)
    {
        mount(myPlugin);
    }
    else
    {
        // Unmount image when deactivated
        IDEUnMount(myPlugin->ide);
    }

    return activate;
}



/****** x-mass.acepansion/GetPrefsPlugin ************************************
*
* NAME
*   GetPrefsPlugin -- (V5) -- GUI
*
* SYNOPSIS
*   STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is used by ACE to know about the current preferences of the
*   plugins in order to save them.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   pool
*     Memory pool to use to allocate the tooltypes.
*
* RESULT
*   Pointer to a NULL terminated string array containing the tooltypes or NULL
*   when no tooltypes.
*
* SEE ALSO
*   CreatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin, APTR pool)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;
    STRPTR *tooltypes = (STRPTR*)AllocPooled(pool, 5*sizeof(STRPTR));

    if(tooltypes)
    {
        STRPTR directoryName;
        STRPTR imageFileName;
        IPTR selected;
        IPTR mode;
        LONG i = 0;

#ifdef __HAIKU__
        directoryName = (char*)myPlugin->STR_DirectoryName->TextControl()->Text();
        imageFileName = (char*)myPlugin->STR_ImageFileName->TextControl()->Text();
        selected = myPlugin->BTN_ImageFileMount->Value();
        mode = myPlugin->CYA_Mode->Value();
#else
        get(myPlugin->STR_DirectoryName, MUIA_String_Contents, &directoryName);
        get(myPlugin->STR_ImageFileName, MUIA_String_Contents, &imageFileName);
        get(myPlugin->BTN_ImageFileMount, MUIA_Selected, &selected);
        get(myPlugin->CYA_Mode,MUIA_Cycle_Active,&mode);
#endif

        tooltypes[i++] = MakeToolType(pool, TOOLTYPE_DIRECTORY, directoryName);
        tooltypes[i++] = MakeToolType(pool, TOOLTYPE_IMAGEFILE, imageFileName);
        if(selected)
        {
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_AUTOMOUNT, NULL);
        }
        switch(mode)
        {
        case MODE_DIRECTORY:
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_MOUNTMODE, TOOLTYPE_MOUNTMODE_DIRECTORY);
            break;
        case MODE_IMAGEFILE:
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_MOUNTMODE, TOOLTYPE_MOUNTMODE_IMAGEFILE);
            break;
        }
        tooltypes[i++] = NULL;
    }

    return tooltypes;
}



/****** x-mass.acepansion/Reset *********************************************
*
* NAME
*   Reset -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Reset(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is called everytime ACE is issuing a bus reset.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*
* RESULT
*
* SEE ALSO
*   Emulate
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Reset(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    // Reset IDE controller
    IDEReset(myPlugin->ide);
}



/****** x-mass.acepansion/Emulate *******************************************
*
* NAME
*   Emulate -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
*
* FUNCTION
*   Function that is called at each microsecond of the emulation step inside ACE.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   *signals
*     Pointer to the I/O signals from the expansion port you can use or alter
*     depending on what you are emulating.
*     The signals bits can be decoded using the following flags:
*       ACE_SIGNALF_INT -- (V1)
*         Maskable interrupt; corresponding to /INT signal from CPC expansion
*         port.
*       ACE_SIGNALF_NMI -- (V3)
*         Non-maskable interrupt; corresponding to /NMI signal from CPC
*         expansion port.
*       ACE_SIGNALF_LPEN -- (V4)
*         Light pen trigger; corresponding to /LPEN signal from CPC expansion
*         port.
*       ACE_SIGNALF_BUS_RESET -- (V6)
*         Bus reset trigger; correspond to /BUSRESET signal from CPC expansion
*         port.
*       ACE_SIGNALF_NOT_READY -- (V6)
*         Not ready trigger; correspond to READY signal from CPC expansion port.
*
* RESULT
*
* SEE ALSO
*   Reset
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(myPlugin->resetTrigger)
    {
        *signals |= ACE_SIGNALF_BUS_RESET;

        myPlugin->resetTrigger = FALSE;
    }
}



/****** x-mass.acepansion/WriteIO *******************************************
*
* NAME
*   WriteIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port write operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O write operation was issued.
*   value
*     Value that was issued on the port.
*
* RESULT
*
* SEE ALSO
*   ReadIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if((port & 0xfff8) == 0xfd08)
    {
        IDEExecute(myPlugin->ide);
        IDEWrite(myPlugin->ide, port & 7, value);
	}
}



/****** x-mass.acepansion/ReadIO ********************************************
*
* NAME
*   ReadIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port read operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O read operation was issued.
*   *value
*     Value to be returned.
*
* RESULT
*
* SEE ALSO
*   WriteIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if((port & 0xfff8) == 0xfd08)
    {
        IDEExecute(myPlugin->ide);
        *value = IDERead(myPlugin->ide, port & 7);
    }
}



/****** x-mass.acepansion/WriteMem ******************************************
*
* NAME
*  WriteMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
*
* FUNCTION
*  Function that is called everytime Z80 is performing a memory write
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which the memory write operation was issued.
*  value
*    Value that has to be written.
*
* RESULT
*  TRUE if the plugins actually cougth the memory write operation so that
*  internal memory will be left unchanged (/RAMDIS is set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  ReadMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL WriteMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE value)
{
    return FALSE;
}



/****** x-mass.acepansion/ReadMem *******************************************
*
* NAME
*  ReadMem -- (V6) -- EMU
*
* SYNOPSIS
*  VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcodeFetch) -- (V7)
*  BOOL ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcode) ** DEPRECATED in V7 **

* FUNCTION
*  Function that is called everytime Z80 is performing a memory read
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which memory read operation was issued.
*  *value
*    Value to be returned.
*    Starting with V7, it contains by default the value that might be read by
*    the CPC if the plugin won't catch the memory read, so that a plugin can
*    only snif the bus.
*  opcodeFetch
*    Boolean which is TRUE when the memory read operation is related to
*    Z80 opcode fetching (/M1 signal). If FALSE, then this is a regular
*    read operation during opcode execution.
*
* RESULT
*  ** DEPRECATED in V7 **
*  TRUE if the plugins actually cougth the memory read operation so that
*  internal memory won't be read (both /ROMDIS and /RAMDIS are set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  WriteMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE *value, UNUSED BOOL opcodeFetch)
{
}



/****** x-mass.acepansion/GetAudio ******************************************
*
* NAME
*   GetAudio -- (V1) -- EMU
*
* SYNOPSIS
*   VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
*
* FUNCTION
*   Function to be used when your plugin needs to mix an audio output with the one
*   from ACE.
*   It is called at each PSG emulation step, which means at 125KHz.
*
* INPUTS
*   *leftSample
*     Pointer to store the left audio sample to be mixed with ACE audio.
*   *rightSample
*     Pointer to store the right audio sample to be mixed with ACE audio.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID GetAudio(UNUSED struct ACEpansionPlugin *plugin, UNUSED SHORT *leftSample, UNUSED SHORT *rightSample)
{
}



/****** x-mass.acepansion/Printer *******************************************
*
* NAME
*   Printer -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WritePrinter(struct ACEpansionPlugin *plugin, UBYTE data, BOOL strobe, BOOL *busy)
*
* FUNCTION
*   Function that is called everytime the CPC is accessing the printer port.
*
* INPUTS
*   data
*     8 bits written value (note that CPC is limited to 7 bits values, only Amstrad Plus
*     can actually provide the 8th bit).
*   strobe
*     Status of the /STROBE signal from the printer port.
*   *busy
*     Busy signal value that can be altered.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Printer(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE data, UNUSED BOOL strobe, UNUSED BOOL *busy)
{
}



/****** x-mass.acepansion/Joystick ******************************************
*
* NAME
*   Joystick -- (V2) -- EMU
*
* SYNOPSIS
*   VOID Joystick(struct ACEpansionPlugin *plugin, BOOL com1, BOOL com2, UBYTE *ioData)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the joystick port.
*   Please note that the joystick port in R/W but you cannot know if the CPC
*   is actually reading or writing to it.
*
* INPUTS
*   com1
*     If com1 is active then the CPC is requesting access to joystick 0.
*   com2
*     If com2 is active then the CPC is requesting access to joystick 1.
*   *ioData
*     If the CPC is reading the joystick port (regular case) ioData will be set
*     to 0xff and you are supposed to set to 0 the bits corresponding to the
*     requested joystick (you should use the ACE_IODATAF_JOYSTICK_XXX defines).
*     Please note the additional ACE_IODATAF_JOYSTICK_PAUSE which is not
*     existing on CPC but that you could use to map the play/pause button of
*     a joystick used in ACE on the 'P' key of the keyboard (or the GX4000
*     pause button).
*     If the CPC is writing the joystick port the ioData will contain the
*     written value to the port and modifying it will have not effect (on a
*     real CPC it could simply destroy the PSG chipset!).
*
* RESULT
    To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
    this function should be used together with AnalogInput().
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Joystick(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL com1, UNUSED BOOL com2, UNUSED UBYTE *ioData)
{
}



/****** x-mass.acepansion/AnalogInput ***************************************
*
* NAME
*   AnalogInput -- (V7) -- EMU
*
* SYNOPSIS
*   VOID AnalogInput(struct ACEpansionPlugin *plugin, UBYTE channel, UBYTE *value)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the analog joystick
*   port of the Amstrad Plus or the GX-4000.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   channel
*     Analog channel number (from 0 to 3).
*   *value
*     Pointer where to store the 6-bit wide value of the data on the channel.
*
* RESULT
*
* NOTES
*   To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
*   this function should be used together with Joystick() which is providing
*   fire buttons (they are shared between both analog and digital joystick
*   ports).
*
* SEE ALSO
*   Joystick()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AnalogInput(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE channel, UNUSED UBYTE *value)
{
}



/****** x-mass.acepansion/AcknowledgeInterrupt ******************************
*
* NAME
*   AcknowledgeInterrupt -- (V1) -- EMU
*
* SYNOPSIS
*   VOID AcknowledgeInterrupt(struct ACEpansionPlugin *plugin, UBYTE *ivr)
*
* FUNCTION
*   Function which is called everytime the Z80 is entering an interrupt.
*
* INPUTS
*   *ivr
*     IVR could be set to the desired value (Interrupt Vector Register).
*
* RESULT
*
* SEE ALSO
*   ReturnInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AcknowledgeInterrupt(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE *ivr)
{
}



/****** x-mass.acepansion/ReturnInterrupt ***********************************
*
* NAME
*   ReturnInterrupt -- (V3) -- EMU
*
* SYNOPSIS
*   VOID ReturnInterrupt(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the Z80 is returning from an interrupt
*   using RETI instruction.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   AcknowledgeInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReturnInterrupt(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** x-mass.acepansion/Cursor ********************************************
*
* NAME
*   Cursor -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Cursor(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the cursor signal from the CRTC is
*   active.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Cursor(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** x-mass.acepansion/HostGamepadList ***********************************
*
* NAME
*   HostGamepadList -- (V7) -- GUI

* SYNOPSIS
*   VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
*
* FUNCTION
*   This function is called everytime a gamepad in plugged or unplugged from
*   the host so that the plugin can know about the available devices. Index
*   from HostGamepadEvent() will refer to this list.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   list
*     NULL terminated array of strings containing the human readable names of
*     the plugged gamepads, the first one being the localized string for "No
*     joystick". Please note that the contents of this list remains valid
*     until the function is invoked again, so that you can use it safely in
*     your code without duplication.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadList(UNUSED struct ACEpansionPlugin *plugin, UNUSED STRPTR *list)
{
}



/****** x-mass.acepansion/HostGamepadEvent **********************************
*
* NAME
*   HostGamepadEvent -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
*
* FUNCTION
*   This function is called everytime the state of a host's gamepad state is
*   changing. It let you handle both analogic and digital devices.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   index
*     Index of the gamepad in gamepad list.
*   buttons
*     State of the gamepad buttons.
*     Buttons bits can be decoded using the ACE_HOSTGAMEPADF_BUTTON_XXX flags.
*   ns
*     Horizontal position of directional buttons from -128 to +127.
*   ew
*     Vertical position of directional buttons from -128 to +127.
*   lx
*     Horizontal position of left stick from -128 to +127.
*   ly
*     Vertical position of left stick from -128 to +127.
*   rx
*     Horizontal position of right stick from -128 to +127.
*   ry
*     Vertical position of right stick from -128 to +127.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE index, UNUSED USHORT buttons, UNUSED BYTE ns, UNUSED BYTE ew, UNUSED BYTE lx, UNUSED BYTE ly, UNUSED BYTE rx, UNUSED BYTE ry)
{
}



/****** x-mass.acepansion/HostMouseEvent ************************************
*
* NAME
*   HostMouseEvent -- (V7) -- GUI

* SYNOPSIS
*   VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)

* FUNCTION
*   Function that is called everytime a host's mouse event occured

* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   buttons
*     State of the mouse buttons.
*     The mouse buttons bits can be decoded using the following flags:
*       ACE_HOSTMOUSEF_BUTTON_MAIN
*         State of main mouse button.
*       ACE_HOSTMOUSEF_BUTTON_SECOND
*         State of second mouse button.
*       ACE_HOSTMOUSEF_BUTTON_MIDDLE
*         State of middle mouse button.
*   deltaX
*     Horizontal mouse move until previous event.
*   deltaY
*     Vertical mouse move until previous event.
*
* RESULT
*
* NOTES
*
* SEE ALSO
      libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostMouseEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE buttons, UNUSED SHORT deltaX, UNUSED SHORT deltaY)
{
}



/****** x-mass.acepansion/HostLightDeviceDiodePulse *************************
*
* NAME
*   HostLightDeviceDiodePulse -- (V7) -- EMU
*
* SYNOPSIS
*   VOID HostLightDeviceDiodePulse(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   This function is called at each photo-diode pulse, when the beam from
*   the screen monitor is just in front of the light device photo-diode.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   HostLightDeviceButton
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceDiodePulse(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** x-mass.acepansion/HostLightDeviceButton *****************************
*
* NAME
*   HostLightDeviceButton -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostLightDeviceButton(struct ACEpansionPlugin *plugin, BOOL pressed)
*
* FUNCTION
*   This function is called everytime the state of the light device button
*   is changing.
*
* INPUTS
*   pressed
*     New state of the light device button.
*
* RESULT
*
* NOTE
*   When a light device plugin is activated, ACE is mapping this button on
*   mouse main button, so that on-screen click will emulate light device
*   button.
*
* SEE ALSO
*   HostLightDeviceDiodePulse
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceButton(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL pressed)
{
}



/*
** X-Mass private functions
*/

#ifdef __HAIKU__
void SettingsWindow::MountHook()
{
    ObtainSemaphore(myPlugin->sema);
    mount(myPlugin);
    myPlugin->resetTrigger = TRUE;
    ReleaseSemaphore(myPlugin->sema);
}
#else
static LONG mountHandler(VOID)
{
	//struct Object *obj = (Object *)REG_A2;
	//struct Msg_MountHandler *msg = (struct Msg_MountHandler *)REG_A1;
	struct Hook *hook = (struct Hook *)REG_A0;
    struct PluginData *myPlugin = (struct PluginData *)hook->h_Data;

    ObtainSemaphore(myPlugin->sema);

    mount(myPlugin);

    myPlugin->resetTrigger = TRUE;

    ReleaseSemaphore(myPlugin->sema);

    return 0;
}
#endif

static VOID mount(struct PluginData *myPlugin)
{
    IPTR mode;

#ifdef __HAIKU__
    mode = myPlugin->CYA_Mode->Value();
#else
    get(myPlugin->CYA_Mode,MUIA_Cycle_Active,&mode);
#endif

    switch(mode)
    {
    case MODE_DIRECTORY:
        {
            STRPTR directoryName;

#ifdef __HAIKU__
            directoryName = (char*)myPlugin->STR_DirectoryName->TextControl()->Text();
#else
            get(myPlugin->STR_DirectoryName, MUIA_String_Contents, &directoryName);
#endif

#ifndef __HAIKU__
            set(myPlugin->common.ap_PrefsWindow, MUIA_Disabled, TRUE);
#endif
            IDEMount(myPlugin->ide, IDE_MOUNT_TYPE_DIR, directoryName);
#ifndef __HAIKU__
            set(myPlugin->common.ap_PrefsWindow, MUIA_Disabled, FALSE);
#endif
        }
        break;
    case MODE_IMAGEFILE:
        {
            IPTR selected;
            STRPTR imageFileName;

#ifdef __HAIKU__
            selected = myPlugin->BTN_ImageFileMount->Value();
            imageFileName = (char*)myPlugin->STR_ImageFileName->TextControl()->Text();
#else
            get(myPlugin->BTN_ImageFileMount, MUIA_Selected, &selected);
            get(myPlugin->STR_ImageFileName, MUIA_String_Contents, &imageFileName);
#endif

            if(selected)
            {
                if(!IDEMount(myPlugin->ide, IDE_MOUNT_TYPE_IMAGE, imageFileName))
                {
#ifdef __HAIKU__
                    puts("oops");
                    myPlugin->BTN_ImageFileMount->SetValue(B_CONTROL_OFF);
#else
                    nnset(myPlugin->BTN_ImageFileMount, MUIA_Selected, FALSE);
#endif
                }
            }
            else
            {
                IDEUnMount(myPlugin->ide);
            }
        }
        break;
    }
}
