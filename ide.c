/*
    Kindly given by Fr�d�ric Coste
    Extracted from CaPriCe Forever
    Copyright (C) 2014-2017 by Fr�d�ric Coste

    Added VVFAT support by Philippe Rimauro
    Copyright (C) 2022
*/

#include <proto/exec.h>
#include <proto/dos.h>

#include <string.h>


extern struct Library *SysBase;
extern struct Library *DOSBase;


#include "ide.h"
#include "vvfat.h"

#define IDE_STATUS_BUSY                 0x80
#define IDE_STATUS_DEV_READY            0x40
#define IDE_STATUS_DATA_REQ             0x08
#define IDE_STATUS_ERROR                0x01

#define IDE_COMMAND_READ_SECTOR         0x20
#define IDE_COMMAND_WRITE_SECTOR        0x30
#define IDE_COMMAND_IDENTIFY_DEVICE     0xEC

#define IDE_DOM_128M_NB_CYLINDERS       251
#define IDE_DOM_128M_NB_HEADS            16
#define IDE_DOM_128M_NB_SECTORS          63

#define IDE_DOM_512M_NB_CYLINDERS      1015
#define IDE_DOM_512M_NB_HEADS            16
#define IDE_DOM_512M_NB_SECTORS          63

#define IDE_DISPLAY_ACCESS_NB_FRAMES      2


struct IDE
{
    struct VVFAT *vvfat;
    BPTR raw_file_handle;

	UBYTE *cacheP;
	UBYTE *end_cacheP;
	UBYTE *currentP;

	ULONG nb_cylinders;
	ULONG nb_heads;
	ULONG nb_sectors_per_cylinder;

	ULONG read_address;
	ULONG read_count;
	ULONG write_address;
	ULONG write_count;

	ULONG display_read_frames;
	ULONG display_write_frames;

	UBYTE registers[8];
	UBYTE reg_error; // TODO
	UBYTE reg_status;
};

static VOID executeCommand(struct IDE *data, UBYTE command);
static ULONG getSectorFromRegisters(struct IDE *data);
static BOOL isFileOperationRequested(struct IDE *data);
static VOID setError(struct IDE *data);
static VOID resumeReadOperation(struct IDE *data);
static VOID resumeWriteOperation(struct IDE *data);

static const UBYTE identify_device_dom_reply_template[] =
{
	0x5A, 0x04, 0xFB, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x02, 0x3F, 0x00, 0x03, 0x00,
	0x50, 0xDC, 0x00, 0x00, 0x4F, 0x44, 0x41, 0x4D, 0x41, 0x33, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
	0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x37, 0x38, 0x01, 0x00, 0x01, 0x00, 0x04, 0x00, 0x55, 0x59,
	0x4E, 0x41, 0x30, 0x31, 0x36, 0x32, 0x51, 0x50, 0x20, 0x49, 0x44, 0x49, 0x20, 0x45, 0x69, 0x44,
	0x6B, 0x73, 0x6E, 0x4F, 0x6F, 0x4D, 0x75, 0x64, 0x65, 0x6C, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x01, 0x00,
	0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x07, 0x00, 0xFB, 0x00, 0x10, 0x00,
	0x3F, 0x00, 0x50, 0xDC, 0x03, 0x00, 0x00, 0x01, 0x50, 0xDC, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00,
	0x02, 0x09, 0x09, 0x19, 0xFF, 0xFF, 0xFF, 0x00, 0x02, 0x00, 0xBE, 0x34, 0xC9, 0x35, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x47, 0x46, 0x58, 0x20, 0x20, 0x20,
	0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x17, 0x03, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x01, 0x4F, 0x46, 0x46, 0x53, 0x45, 0x54, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x10, 0x00, 0x00, 0x17, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x4F, 0x46, 0x46, 0x53,
	0x45, 0x54, 0x20, 0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x17, 0x03,
};


struct IDE * IDECreate(VOID)
{
    struct IDE *data = (struct IDE*)AllocVec(sizeof(struct IDE), MEMF_PUBLIC | MEMF_CLEAR);

    if(data)
    {
        // 128MB DoM specifications (only used for image mount)
        data->nb_cylinders = IDE_DOM_128M_NB_CYLINDERS;
        data->nb_heads = IDE_DOM_128M_NB_HEADS;
        data->nb_sectors_per_cylinder = IDE_DOM_128M_NB_SECTORS;

        // Create sector cache
        data->cacheP = (UBYTE*)AllocVec(0x200, MEMF_PUBLIC);
        data->end_cacheP = data->cacheP + 0x200;

        data->raw_file_handle = (BPTR)0;
        data->vvfat = NULL;

        // Test all went good
        if(data->cacheP == NULL)
        {
            IDEDestroy(data);
            data = NULL;
        }
    }

    return data;
}

VOID IDEDestroy(struct IDE *data)
{
    if(data)
    {
        VVFAT_Destroy(data->vvfat);
        Close(data->raw_file_handle);
        FreeVec(data->cacheP);
        FreeVec(data);
    }
}

BOOL IDEMount(struct IDE *data, UBYTE mountType, CONST_STRPTR path)
{
    // First unmount any previously mounted stuff
    IDEUnMount(data);

    if(path)
    {
        switch(mountType)
        {
        case IDE_MOUNT_TYPE_IMAGE:
            // Open the operation file (create it if not already there)
            data->raw_file_handle = Open(path, MODE_READWRITE);

            if(data->raw_file_handle)
            {
                struct FileInfoBlock *fib = (struct FileInfoBlock*)AllocDosObject(DOS_FIB, NULL);

                if(fib)
                {
                    if(ExamineFH(data->raw_file_handle, fib))
                    {
                        LONG imageSize = 0x200 * data->nb_cylinders * data->nb_heads * data->nb_sectors_per_cylinder;

                        // We should fail if the file is too small and we cannot expand it
                        if(fib->fib_Size < imageSize)
                        {
                            if(SetFileSize(data->raw_file_handle, imageSize, OFFSET_BEGINNING) < 0)
                            {
                                Close(data->raw_file_handle);
                                data->raw_file_handle = (BPTR)0;
                            }
                        }
                    }

                    if(fib)
                    {
                        FreeDosObject(DOS_FIB, fib);
                    }
                }
            }
            break;

        case IDE_MOUNT_TYPE_DIR:
            // Create VVFAT instance
            data->vvfat = VVFAT_Create(path, NULL);

            /*if(data->vvfat)
            {
                LONG index;
                UBYTE buffer[0x200];
                BPTR dump = Open("RAM:dump32", MODE_NEWFILE);

                if(dump) for(index=0; index<2*1024*8; index++)
                {
                    VVFAT_ReadData(data->vvfat, buffer, index * 0x200, 0x200);
                    Write(dump, buffer, 0x200);
                }
                Close(dump);
            }*/
            break;
        }
    }

    if(data->raw_file_handle || data->vvfat)
    {
        IDEReset(data);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

VOID IDEUnMount(struct IDE *data)
{
    // Release any currently used file
    Close(data->raw_file_handle);
    data->raw_file_handle = (BPTR)0;
    // Release any currently used directory
    VVFAT_Destroy(data->vvfat);
    data->vvfat = NULL;
}

VOID IDEReset(struct IDE *data)
{
    memset(data->registers, 0, sizeof(data->registers));
    data->reg_error = 0;

    if(data->raw_file_handle || data->vvfat)
    {
        // Device is ready
        data->reg_status = IDE_STATUS_DEV_READY;
    }

	data->read_address =
	data->read_count =
	data->write_address =
	data->write_count = 0;
}

UBYTE IDERead(struct IDE *data, UBYTE reg_index)
{
    UBYTE ret_val;

    switch(reg_index)
    {
        case 0:
            // Pending read operation ?
            if(data->read_count)
            {
                ret_val = *(data->currentP++);

                // End of transfer
                if(data->currentP == data->end_cacheP)
                {
                    data->reg_status &= ~(IDE_STATUS_DATA_REQ);

                    // Still something to read
                    data->read_count -= 0x200;
                    if(data->read_count)
                    {
                        // Request next sector READ
                        data->read_address += 0x200;
                        data->currentP = data->cacheP;
                        data->display_read_frames = IDE_DISPLAY_ACCESS_NB_FRAMES;

                        // Ready data from IDE
                        data->reg_status |= (IDE_STATUS_BUSY);
                    }
                }
            }
            else
            {
                // To be checked!
                ret_val = 0xff;
            }
            break;

        case 1:
            ret_val = data->reg_error;
            break;

        case 7:
            ret_val = data->reg_status;
            break;

        default:
            ret_val = data->registers[reg_index];
    }

    return ret_val;
}

VOID IDEWrite(struct IDE *data, UBYTE reg_index, UBYTE val)
{
    data->registers[reg_index] = val;

    switch(reg_index)
    {
        case 0:
            // Pending write operation ?
            if(data->write_count)
            {
                *(data->currentP++) = val;

                // End of transfer
                if(data->currentP == data->end_cacheP)
                {
                    data->reg_status &= ~(IDE_STATUS_DATA_REQ);
                    data->reg_status |= (IDE_STATUS_BUSY);
                }
            }
            break;

        case 7:
            executeCommand(data, val);
            break;
    }
}

VOID IDEExecute(struct IDE *data)
{
    // IDE is BUSY
    if(isFileOperationRequested(data))
    {
        // Perform file operation
        if(data->read_count)
        {
            if((data->raw_file_handle
             && Seek(data->raw_file_handle, data->read_address, OFFSET_BEGINNING) != -1
             && Read(data->raw_file_handle, data->cacheP, 0x200) == 0x200)
            || (data->vvfat
             && VVFAT_ReadData(data->vvfat, data->cacheP,  data->read_address, 0x200)))
            {
                // Resume IDE Operation
                resumeReadOperation(data);
            }
            else
            {
                // Raise ERROR
                setError(data);
            }
        }
        else if(data->write_count)
        {
            if((data->raw_file_handle
             && Seek(data->raw_file_handle, data->write_address, OFFSET_BEGINNING) != -1
             && Write(data->raw_file_handle, data->cacheP, 0x200) == 0x200)
            || (data->vvfat
             && VVFAT_WriteData(data->vvfat, data->cacheP, data->write_address, 0x200)))
            {
                // Resume IDE Operation
                resumeWriteOperation(data);
            }
            else
            {
                // Raise ERROR
                setError(data);
            }
        }
    }
}

/*
 * Private functions
 */

static VOID executeCommand(struct IDE *data, UBYTE command)
{
    ULONG nb_sectors;

    switch(command)
    {
        case IDE_COMMAND_READ_SECTOR:
            // Prepare read operation
            data->read_address = getSectorFromRegisters(data) * 0x200;
            nb_sectors = data->registers[2];
            if(!nb_sectors) nb_sectors = 256;
            data->read_count = nb_sectors * 0x200;

            data->currentP = data->cacheP;
            data->display_read_frames = IDE_DISPLAY_ACCESS_NB_FRAMES;

            // Ready data from IDE
            data->reg_status |= (IDE_STATUS_BUSY);
            data->reg_status &= ~(IDE_STATUS_ERROR);
            break;

        case IDE_COMMAND_WRITE_SECTOR:
            // Prepare write opration
            data->write_address = getSectorFromRegisters(data) * 0x200;
            nb_sectors = data->registers[2];
            if(!nb_sectors) nb_sectors = 256;
            data->write_count = nb_sectors * 0x200;

            data->currentP = data->cacheP;
            data->display_write_frames = IDE_DISPLAY_ACCESS_NB_FRAMES;

            // Data are ready to be written
            data->reg_status |= IDE_STATUS_DATA_REQ;
            data->reg_status &= ~(IDE_STATUS_ERROR);
            break;

        case IDE_COMMAND_IDENTIFY_DEVICE:
            //
            // Prepare device identity
            //
            // Fill with default template
            CopyMem((CONST APTR)identify_device_dom_reply_template, data->cacheP, 0x200);
            // Word 1 : Set Number of logical cylinders
            *(data->cacheP + (1*2) + 0) = (data->nb_cylinders & 0xff);
            *(data->cacheP + (1*2) + 1) = ((data->nb_cylinders >> 8) & 0xff);
            // Word 3 : Set Number of logical heads
            *(data->cacheP + (3*2) + 0) = (data->nb_heads & 0xff);
            *(data->cacheP + (3*2) + 1) = ((data->nb_heads >> 8) & 0xff);
            // Word 6 : Set Number of logical sector per logical track
            *(data->cacheP + (6*2) + 0) = (data->nb_sectors_per_cylinder & 0xff);
            *(data->cacheP + (6*2) + 1) = ((data->nb_sectors_per_cylinder >> 8) & 0xff);
            // Word 54 : Set Number of current logical cylinders
            *(data->cacheP + (54*2) + 0) = (data->nb_cylinders & 0xff);
            *(data->cacheP + (54*2) + 1) = ((data->nb_cylinders >> 8) & 0xff);
            // Word 55 : Set Number of current logical heads
            *(data->cacheP + (55*2) + 0) = (data->nb_heads & 0xff);
            *(data->cacheP + (55*2) + 1) = ((data->nb_heads >> 8) & 0xff);
            // Word 56 : Set Number of current logical sector per logical track
            *(data->cacheP + (56*2) + 0) = (data->nb_sectors_per_cylinder & 0xff);
            *(data->cacheP + (56*2) + 1) = ((data->nb_sectors_per_cylinder >> 8) & 0xff);
            // Word 60&61 : Current capacity in sectors
            nb_sectors = data->nb_cylinders * data->nb_heads * data->nb_sectors_per_cylinder;
            *(data->cacheP + (60*2) + 0) = ((nb_sectors >> 0) & 0xff);
            *(data->cacheP + (60*2) + 1) = ((nb_sectors >> 8) & 0xff);
            *(data->cacheP + (61*2) + 0) = ((nb_sectors >> 16) & 0xff);
            *(data->cacheP + (61*2) + 1) = ((nb_sectors >> 24) & 0xff);

            data->read_count = 0x200;
            data->currentP = data->cacheP;

            // Data are ready to be read
            data->reg_status |= IDE_STATUS_DATA_REQ;
            data->reg_status &= ~(IDE_STATUS_ERROR | IDE_STATUS_BUSY);
            break;

        // Unknown command
        default:
            // ERROR
            data->reg_status |= IDE_STATUS_ERROR;
            break;
    }
}

static ULONG getSectorFromRegisters(struct IDE *data)
{
    ULONG Sector;

    // LBA ?
    if(data->registers[6] & 0x40)
    {
        Sector = (data->registers[6] & 0x0f);
        Sector = Sector << 8;
        Sector |= data->registers[5];
        Sector = Sector << 8;
        Sector |= data->registers[4];
        Sector = Sector << 8;
        Sector |= data->registers[3];
    }
    else
    {
        // CHS => LBA
        // NT = Nombre Tetes
        // NS = Nombre secteurs
        // A = ( C * NT * NS ) + ( H * NS ) + S - 1
        Sector  = (((ULONG)data->registers[5] << 8) + (ULONG)data->registers[4])
                * data->nb_heads * data->nb_sectors_per_cylinder;
        Sector += (data->registers[6] & 0x0f) * data->nb_sectors_per_cylinder;
        Sector += data->registers[3] - 1;
    }

    return Sector;
}

static BOOL isFileOperationRequested(struct IDE *data)
{
    return data->reg_status & IDE_STATUS_BUSY;
}

static VOID setError(struct IDE *data)
{
    data->reg_status |= (IDE_STATUS_ERROR);
}

static VOID resumeReadOperation(struct IDE *data)
{
    // IDE is no longer BUSY
    data->reg_status &= ~(IDE_STATUS_BUSY);

    // Data are ready to be read
    data->reg_status |= IDE_STATUS_DATA_REQ;
}

static VOID resumeWriteOperation(struct IDE *data)
{
    // IDE is no longer BUSY
    data->reg_status &= ~(IDE_STATUS_BUSY);

    // Still something to write
    data->write_count -= 0x200;
    if(data->write_count)
    {
        // Request WRITE next sector
        data->write_address += 0x200;
        data->currentP = data->cacheP;

        data->display_write_frames = IDE_DISPLAY_ACCESS_NB_FRAMES;

        // Data are ready to be written
        data->reg_status |= IDE_STATUS_DATA_REQ;
        data->reg_status &= ~(IDE_STATUS_ERROR);
    }
}

