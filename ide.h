/*
    Kindly given by Fr�d�ric Coste
    Extracted from CaPriCe Forever
    Copyright (C) 2014-2017 by Fr�d�ric Coste

    Added VVFAT support by Philippe Rimauro
    Copyright (C) 2022
*/

#ifndef LOCAL_IDE_H
#define LOCAL_IDE_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#define IDE_MOUNT_TYPE_IMAGE 0
#define IDE_MOUNT_TYPE_DIR   1

struct IDE;

struct IDE * IDECreate(VOID);
VOID IDEDestroy(struct IDE *data);

VOID IDEReset(struct IDE *data);

BOOL IDEMount(struct IDE *data, UBYTE mountType, CONST_STRPTR path);
VOID IDEUnMount(struct IDE *data);

UBYTE IDERead(struct IDE *data, UBYTE reg_index);
VOID IDEWrite(struct IDE *data, UBYTE reg_index, UBYTE val);
VOID IDEExecute(struct IDE *data);

#endif /* LOCAL_IDE_H */
