/*
    Virtual VFAT image support (shadows a local directory)
    Ported from Bochs Project
        - Switched back to plain C (C++ seriously?)
        - Fixed endianess issues
        - Added several missing checks in case of memory allocation failure
        - Fixed array out of range in long file name handling
        - Fixed invalid sectors per cluster computation for small FAT32 disk images
        - Fixed deleted files handling during resynchronization
        - Removed libc dependecies in favour of Amiga shared libraries
        - Removed path manual handling in favour of locks
        - Use directory date for volume date
        - Replaced redolog stuff by a simple hashtable
        - Fixed FAT sectors to use the hashtable as well

    Copyright (C) 2004,2005 by Johannes E. Schindelin (QEMU)
    Copyright (C) 2010-2012  The Bochs Project
    Copyright (C) 2022 by Philippe Rimauro
*/

#ifndef VVFAT_H
#define VVFAT_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

#define TYPE_FAT12 12
#define TYPE_FAT16 16
#define TYPE_FAT32 32

struct VVFAT_Configuration
{
    UWORD cylinders;
    UWORD heads;
    UWORD sectors_per_cylinder;

    UBYTE type_of_fat;
};

struct VVFAT;

struct VVFAT * VVFAT_Create(CONST_STRPTR dirName, struct VVFAT_Configuration *cfg);
VOID VVFAT_Destroy(struct VVFAT *v);

BOOL VVFAT_ReadData(struct VVFAT *v, APTR buffer, UQUAD offset, ULONG size);
BOOL VVFAT_WriteData(struct VVFAT *v, const APTR buffer, UQUAD offset, ULONG size);

#endif
