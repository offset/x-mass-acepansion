## Version $VER: x-mass.acepansion.catalog 2.0 (02.10.2022)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a X-Mass
Brancher une X-Mass
X-Mass anschlie�en
Conectar un X-Mass
;
MSG_TITLE
X-Mass
X-Mass
X-Mass
X-Mass
;
MSG_MENU_PREFS
X-Mass configuration...
Configuration de la X-Mass...
X-Mass-Konfiguration ...
Configuraci�n X-Mass...
;
MSG_IMAGE_FILE_GROUP
Hard disc image file configuration
Configuration du fichier image du disque dur
Konfiguration der Festplatten-Abbilddatei
Configuraci�n del fichero imagen del disco duro
;
MSG_IMAGE_FILE
Image file:
Fichier image :
Abbilddatei:
Fichero imagen:
;
MSG_IMAGE_ASL_REQUEST
Choose the image file to use or to create...
Choisissez le fichier image � utiliser ou � cr�er...
W�hle die zu verwendende oder zu erstellende Abbilddatei ...
Eliga el fichero de la imagen a usar � crear...
;
MSG_IMAGE_FILE_HELP
Name of the image file to use as the X-Mass hard disc. A new empty one will be created if the file does not exist.
Nom de l'image disque � utiliser en tant que disque dur de la X-Mass. Un nouveau fichier vide sera cr�� s'il n'existe pas d�j�.
Name der Abbilddatei, die als X-Mass-Festplatte verwendet werden soll. Wenn die Datei nicht existiert, wird eine neue, leere Datei erstellt.
Nombre del fichero de la imagen a usar como disco duro X-Mass. Un nuevo fichero ser� creado si el fichero no existe.
;
MSG_IMAGE_MOUNT
  Mount / Create\x20\x20
  Monter / Cr�er\x20\x20
  Anmelden / Erstellen\x20\x20
  Montar / Crear\x20\x20
;
MSG_IMAGE_MOUNT_HELP
Mount or create (if it does not exists yet) the image file.
Monter ou cr�er (s'il n'existe pas d�j�) l'image disque.
Anmelden oder erstellen (falls noch nicht vorhanden) der Abilddatei.
Montar � crear (si no existe) el fichero imagen.
;
MSG_IMAGE_NOTICE
When mounted, non existing images will be created automatically (they will have a size of 128MB).
Lorsqu'elle est mont�e, si elle n'existe pas, l'image sera cr�� automatiquement (elle aura une taille de 128Mo).
Beim Anmelden werden automatisch nicht vorhandene Abbilder erstellt (sie haben eine Gr��e von 128 MB).
Cuando est� montado, la imagenes que no existan ser�n creadas autom�ticamente (tendr�n un tama�o de 128MB).
;
MSG_DIRECTORY_GROUP
Hard disc directory configuration
Configuration du r�pertoire du disque dur
Konfiguration des Festplattenverzeichnisses
Configuraci�n del directorio del disco duro
;
MSG_DIRECTORY
Directory:
R�pertoire :
Verzeichnis:
Directorio:
;
MSG_DIRECTORY_ASL_REQUEST
Choose the directory to use as the hard disk contents...
Choisissez le r�pertoire � utiliser en tant que contenu du disque dur...
W�hle das Verzeichnis, das als Festplatteninhalt verwendet werden soll ...
Eliga el directorio a usar como disco duro de contenidos...
;
MSG_DIRECTORY_HELP
Name of the directory to use as the X-Mass hard disc contents. It will be automatically synchronized with potential updates at every reset.
Nom du r�pertoire � utiliser en tant que contenu du disque dur de la X-Mass. Il sera automatiquement synchronis� � chaque reset avec les mises � jours �ventuelles.
Name des Verzeichnisses, das als Inhalt der X-Mass-Festplatte verwendet werden soll. Es wird bei jedem Zur�cksetzen automatisch mit m�glichen Aktualisierungen synchronisiert.
Nombre del directorio a usar como disco duro de contenidos para X-Mass. Autom�ticamente se sincronizar� con actualizaciones con cada reinicio.
;
MSG_MODE_GROUP
Hard disk emulation type
Type d'�mulation de disque dur
Typ der Festplattenemulation
Tipo de emulaci�n del disco duro
;
MSG_MODE_HELP
Choose if you want to emulate the hard from a directory or from an image file.
Choisissez si vous voulez �muler le disque dur � partir d'une image disque ou d'un r�pertoire.
W�hle, ob die Festplatte aus einem Verzeichnis oder aus einer Abbilddatei emuliert werden soll.
Eliga si quiere emular el disco duro a partir de una imagen de disco � un directorio.
;
MSG_MODE_DIRECTORY
Use a directory
Utiliser un r�pertoire
Verwende ein Verzeichnis
Use un directorio
;
MSG_MODE_IMAGEFILE
Use an image file
Utiliser une image disque
Verwende eine Abbilddatei
Use un fichero de imagen
;
MSG_SYNCHRONIZE
  Force synchronization\x20\x20
  Forcer la synchronisation\x20\x20
  Synchronisierung erzwingen\x20\x20
  Forzar la sincronizaci�n\x20\x20
;
MSG_SYNCHRONIZE_HELP
Hard disc synchronzation with its source directory or image is automatically done when ACE exits or when X-Mass is disabled, but you can also manually trigger it by pressing this button.
La synchronisation du disque dur �mul� avec son r�pertoire ou son image source a lieu automatiquement lorsque ACE quitte ou que la X-Mass est d�sactiv�e, mais vous avez aussi la possibilit� de la \
provoquer manuellement en pressant ce bouton.
Die Synchronisierung der Festplatte mit dem Quellverzeichnis oder Abbild erfolgt automatisch, wenn ACE beendet wird oder wenn X-Mass deaktiviert ist, kann aber auch manuell durch Dr�cken dieser \
Schaltfl�che ausgel�st werden.
La sincronizaci�n del disco duro con el directorio de origen � la imagen de origen hechos autom�ticamente con la salida del ACE  o la desactivaci�n del X-Mass, tambi�n tiene la opci�n de provocarlo \
presionando este bot�n. manualmente.
;
