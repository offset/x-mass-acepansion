@DATABASE x-mass.acepansion.guide
@$VER: X-Mass 2.1 (21.10.2023)
@INDEX Main
@TOC Main
@AUTHOR Philippe Rimauro
@MASTER ACE.guide

@NODE Main
@TITLE "ACE � Plugin de expansi�n para el X-Mass"


    @{b}Plugin de expansi�n X-Mass 2.1@{ub}
    Plugin para emular la tarjeta X-Mass.

    Copyright � 2020-2023 Philippe Rimauro.
    All rights reserved.

    Basado en la emulaci�n de X-Mass creada por Fr�d�ric 'Fredouille' Coste y sobre
    el driver de QEMU y el de Bochs Virtual VFAT.


    @{u}Descripci�n@{uu}
    Este plugin emula la tarjeta de expansi�n X-Mass de Tot0. Para la emulaci�n del
    contenido del disco duro, puede usarse � los contenidos de un  directorio de la
    m�quina hue�sped (para el Amstrad DOS  soportando �nicamente VFAT32), � la ima-
    gen de un disco duro.


    @{u}Tooltypes@{uu}

    @{b}*@{ub} @{b}MOUNTMODE@{ub}: Modo de emulaci�n del disco duro a usar. "Directory" para usar un
      directorio, "ImageFile" para una imagen de disco.
      Por defecto, la emulaci�n ser� atrav�s del directorio usado.

    @{b}*@{ub} @{b}DIRECTORY@{ub}: Nombre del directorio  a usar como  disco duro, cuando el modo de
      emulaci�n "Directory" es usado.
      Por defecto, el directorio del ACE "Share" es el usado.

    @{b}*@{ub} @{b}IMAGEFILE@{ub}: nombre del fichero con la imagen del disco duro a usar.
      El fichero que  usa ACE  por defecto  est� en  la  carpeta "Share",  llamado
      "X-Mass.img".

    @{b}*@{ub} @{b}AUTOMOUNT@{ub}: 1 si  la imagen  a sido  montada  � creada  en la  activaci�n del
      m�dulo de expansi�n, 0 si no.
      Por defecto la imagen no se monta autom�ticamente.


   @{u}Programaci�n@{uu}

     @{b}C�digo:@{ub}
        @{b}*@{ub} Fr�d�ric @{i}'Fredouille'@{ui} Coste - @{"mailto:coste.frederic@free.f" system "openurl mailto:coste.frederic@free.f"}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Icono:@{ub}
        @{b}*@{ub} Christophe @{i}'Highlander'@{ui} Delorme - @{"mailto:chris.highlander@free.fr" system "openurl mailto:chris.highlander@free.fr"}
     @{b}Documentaci�n:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
     @{b}Traductores:@{ub}
        @{b}*@{ub} Philippe @{i}'OffseT'@{ui} Rimauro - @{"mailto:offset@cpcscene.net" system "openurl mailto:offset@cpcscene.net"}
        @{b}*@{ub} Juan Carlos Herran Martin - @{"mailto:juancarlos@morguesoft.eu" system "openurl mailto:juancarlos@morguesoft.eu"}
        @{b}*@{ub} Stefan @{i}'polluks'@{ui} Haubenthal - @{"mailto:polluks@sdf.lonestar.org" system "openurl mailto:polluks@sdf.lonestar.org"}

@ENDNODE

